

#include "../inc/replay.hpp"
#include "../inc/replayGUI.hpp"
#include <iostream>
#include <TApplication.h>
#include <TROOT.h>

Replay* replayScript(std::string, std::string, std::string);

int main(int argc, char** argv)
{
	//create and initialize variables
	time_t timer{time(nullptr)};
	std::string trigDir{};
	std::string wfDir{};
	std::string paramsDir{};
	TApplication* oaApp{};
	Replay* rep{};
	Int_t argcT{1};
	Char_t* argvT[1];

	//assure that 3 arguments were provided
	if(argc == 4){
  		//create argument array for TApplication instance
		argvT[0] = {new Char_t[strlen(argv[0]) + 1]};
		for(Int_t i{}; i < strlen(argv[0]) + 1; ++i)
			argvT[0][i] = {argv[0][i]};

		//initialize variables
		for(Short_t i{}; i < strlen(argv[1]); ++i)
			trigDir += {argv[1][i]};
		for(Short_t i{}; i < strlen(argv[2]); ++i)
			wfDir += {argv[2][i]};
		for(Short_t i{}; i < strlen(argv[3]); ++i)
			paramsDir += {argv[3][i]};
    

		//create instance of TApplication
		oaApp = {new TApplication{"Online Analysis for the Nab Experiment", &argcT, argvT}};

		//assure the user is not running in batch mode
		if(gROOT->IsBatch()){
			//send error message to stdout, cleanup, and return nonzero status
			std::cout << "\n  Error - Cannot Run In Batch Mode\n";
			delete[] argvT[0];
			delete oaApp;
			std::cout << "\n  Run Time: " << difftime(time(nullptr),timer) << " s\n\n";
			return 2;
		}

		//run analysis on initial data set and start GUI if successful
		rep = {replayScript(trigDir, wfDir, paramsDir)};
		if(rep) oaApp->Run(true);
		else{
			//send error message to stdout, cleanup, and return nonzero status
			std::cout << "\n  Error - replayScript Returned Null Result\n";
			delete[] argvT[0];
			delete oaApp;
			std::cout << "\n  Run Time: " << difftime(time(nullptr),timer) << " s\n\n";
			return 3;
		}
	}
	else{
		//send error message to stdout and return nonzero status
		//std::cout << "\n  Error - 2 Arguments Required: Trigger File Name and Waveform File Name\n";
		std::cout << "\n  Error - 3 Arguments Required: Trigger, Waveform, and Parameter Directories\n";
		std::cout << "\n  Run Time: " << difftime(time(nullptr),timer) << " s\n\n";
		return 1;
	}

	//cleanup, report run time to stdout, and return zero
	delete[] argvT[0];
	delete rep;
	delete oaApp;
	std::cout << "\n  Run Time: " << difftime(time(nullptr),timer) << " s\n\n";
	return 0;
}


Replay* replayScript(std::string trigDir, std::string wfDir, std::string paramsDir)
{
	//create and initialize variables
  	Replay* rep = {new Replay{}};

	//extract calibration and spectrometer mapping data from files
	//** calibration member function is disabled
	rep->readSpectMap("calib/spectrometer.map");

	//set the directory locations
	rep->setTrigDir(trigDir);
	rep->setWfDir(wfDir); //LDM3
	rep->setParamsDir(paramsDir); //LDM3
	rep->updateFileInfo();

	//extract the data from the datafiles
	rep->readTrigFile();
	rep->readWfFile(); //LDM3
	rep->readParamsFile(); //LDM3
	
	//assure that no errors have occured before proceeding
	if(!rep->reportError())
	{
		//process data
		rep->partIdent();
		rep->computeProtData();
		if(!rep->getpDflag()) rep->extractCorPar(); //LDM3... only run this function if we didn't get a flag from the last one
		
		//create and fill GUI
		rep->createGUI();
		rep->fillGUI();
	}
  else{
		//send error message to stdout
		if(rep->reportError() & 1)
			std::cout << "\n  Cannot Open Trigger Data File: " << rep->getTrigFile() << "\n";
		if(rep->reportError() & 2)
			std::cout << "\n  Either Cannot Open Spectrometer Mapping File\n  or the File's "
			"Size is Incorrect: calib/spectrometer.map\n";
		//** if(rep->reportError() & 4)
    	//** std::cout << "\nCannot Open Calibration File:";

		//cleanup
    	delete rep;
    	rep = {};
  }

	//return Replay object or nullptr
  	return rep;
}
