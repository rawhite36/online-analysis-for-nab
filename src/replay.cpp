//Replay analysis class


#include "../inc/replay.hpp"
#include "../inc/replayGUI.hpp"
#include <fstream>
#include <cmath>
#include <TH2Poly.h>
#include <TF1.h>
#include <TStyle.h>
#include <TPaveText.h>
#include "THStack.h" //LDM3

UChar_t Replay::errorFlag{};

//----------------------------------------------------------------------------------------

Replay::trigData::~trigData()
{
	//cleanup
	if(bdNum)
	{
		delete[] bdNum;
		delete[] chNum;
		delete[] timeReq;
		delete[] adcVal;
	}
}

//----------------------------------------------------------------------------------------

void Replay::trigData::readData()
{
	//open new trig-file and cleanup data from old trig-file
	std::ifstream finTrigg{fileName, std::ifstream::binary | std::ifstream::ate};
	
	//assure stream state is good
	if(finTrigg.good())
	{
		if(bdNum)
		{
			delete[] bdNum;
			delete[] chNum;
			delete[] timeReq;
			delete[] adcVal;
		}
		/*
			Note the format of the .trigg files
			Header: 
				- 4byte int: file version 
				- 4byte int: header length (not including these first 8bytes)
				- 4byte int: Git Branch string lengh(N)
				- N byte string: Git Branch
				- 4byte int: Git Branch Hash length(M)
				- M byte string: The git hash for this version of the code
				- 8byte timestamp that represents when the run button was pressed
				- 8byte FPGA timestamp retrieved when the start button was pressed
			.trigg file:
				- 8byte unsigned long: FPGA timestamp
				- 1byte unsigned number: board*8 + channel (identifies which pixel this comes from)
					(note that there are 127 channels, so there are 16 boards?)
				- 2byte unsigned short: energy of the trigger (ADC units)
		//*/
		//determining the size of the header by reading it in line by line:
		finTrigg.seekg(0,finTrigg.beg); //starting from the beginning
		
		UInt_t version, headerLength_fromFile;
		finTrigg.read(reinterpret_cast<Char_t*>(&version),sizeof (UInt_t));
		finTrigg.read(reinterpret_cast<Char_t*>(&headerLength_fromFile),sizeof (UInt_t));
		/*This section is unnecessary sinc we are given the header length
		Int_t gitBranchStringLen_N;
		finTrigg.read(reinterpret_cast<Char_t*>(&gitBranchStringLen_N),sizeof (Int_t));
		UChar_t gitBranch [gitBranchStringLen_N];
		finTrigg.read(reinterpret_cast<Char_t*>(&gitBranch),sizeof (gitBranch));
		Int_t gitBranchHashLen_M;
		finTrigg.read(reinterpret_cast<Char_t*>(&gitBranchHashLen_M),sizeof (Int_t));
		UChar_t gitHash[gitBranchHashLen_M];
		finTrigg.read(reinterpret_cast<Char_t*>(&gitHash),sizeof (gitHash));
		Long_t runTimestamp, FPGAtimestamp;
		finTrigg.read(reinterpret_cast<Char_t*>(&runTimestamp),sizeof (Long_t));
		finTrigg.read(reinterpret_cast<Char_t*>(&FPGAtimestamp),sizeof (Long_t));
		
		Int_t headerLength = sizeof(gitBranchStringLen_N) + sizeof(gitBranch) + sizeof(gitBranchHashLen_M)
			+ sizeof(gitHash) + sizeof(runTimestamp) + sizeof(FPGAtimestamp);
		//*/
		
		Int_t headerLength = headerLength_fromFile;

		//find number of entries in trigg-file and instantiate correctly sized arrays
		Int_t triggEventSize = 8+1+2; //long+char+short
		Int_t numBoards = 16;
		Int_t chanPerBoard = 8;
		finTrigg.seekg(0,finTrigg.end);
		numEnt = {finTrigg.tellg()}; //placeholder to be replaced with the real # later
		numEnt = {(numEnt - headerLength)/triggEventSize};
		std::cout<<"numEnt="<<numEnt<<std::endl;
		timeReq = {new ULong64_t[numEnt]};
		bdNum = {new Int_t[numEnt]};
		chNum = {new UChar_t[numEnt]};
		adcVal = {new UShort_t[numEnt]};
		UChar_t bcNum; //board channel number
		
		
		//fill data arrays from trig-file
		finTrigg.seekg(headerLength+8,finTrigg.beg); //adding 8 for the 2 ints (version and headerlength variables)
		for(Long64_t i{}; i < numEnt; ++i){
			finTrigg.read(reinterpret_cast<Char_t*>(&timeReq[i]), sizeof (ULong64_t));
			finTrigg.read(reinterpret_cast<Char_t*>(&bcNum), sizeof (UChar_t));
			finTrigg.read(reinterpret_cast<Char_t*>(&adcVal[i]), sizeof (UShort_t));
			
			//setting the channel and board numbers
			chNum[i]=Int_t(bcNum)%chanPerBoard;
			bdNum[i]=Int_t(bcNum)/chanPerBoard;			
		}
	}
	else
		errorFlag = {1};
}

//----------------------------------------------------------------------------------------

Replay::partData::~partData()
{
	if(bdNum[0])
	{
		//cleanup
		delete[] bdNum[0];
		delete[] chNum[0];
		delete[] timeReq[0];
		delete[] adcVal[0];
		delete[] partNum[0];
		delete[] bdNum[1];
		delete[] chNum[1];
		delete[] timeReq[1];
		delete[] adcVal[1];
		delete[] partNum[1];
		delete[] protMom;
		delete[] cosTheta;
	}
}

//----------------------------------------------------------------------------------------

void Replay::partData::setData(Long64_t x)
{
	if(bdNum[0])
	{
		//cleanup
		delete[] bdNum[0];
		delete[] chNum[0];
		delete[] timeReq[0];
		delete[] adcVal[0];
		delete[] partNum[0];
		delete[] bdNum[1];
		delete[] chNum[1];
		delete[] timeReq[1];
		delete[] adcVal[1];
		delete[] partNum[1];
		delete[] protMom;
		delete[] cosTheta;
	}

	//re-initialize data members
	numEnt = {x};
	bdNum[0] = {new Int_t[x]};
	chNum[0] = {new UChar_t[x]};
	timeReq[0] = {new ULong64_t[x]};
	adcVal[0] = {new UShort_t[x]};
	partNum[0] = {new Short_t[x]};
	bdNum[1] = {new Int_t[x]};
	chNum[1] = {new UChar_t[x]};
	timeReq[1] = {new ULong64_t[x]};
	adcVal[1] = {new UShort_t[x]};
	partNum[1] = {new Short_t[x]};
	protMom = {new Double_t[x]};
	cosTheta = {new Double_t[x]};
}

//----------------------------------------------------------------------------------------

//LDM3
void Replay::waveformData::readData(Long64_t numEnt, Long64_t wfNum){
	std::ifstream finWf{fileName, std::ifstream::binary | std::ifstream::ate};
	
	//assure stream state is good
	if(finWf.good())
	{
		if(wfArr || wfLenArr){
			delete[] wfLenArr;
			for (Int_t i{}; i<500; i++) delete[] wfArr[i];
		}
	
		/*
			Note the format of the waveform files
			Main Header: 
				- 4byte int: file version
				- 4byte int: header length (not including these first 8bytes)
				- 4byte int: Git Branch string lengh(N)
				- N byte string: Git Branch
				- 4byte int: Git Branch Hash length(M)
				- M byte string: The git hash for this version of the code
				- 8byte timestamp that represents when the run button was pressed
				- 8byte FPGA timestamp retrieved when the start button was pressed
			Waveform file header:
				- 2byte unsigned short: Pre-trigger offset (maybe not needed?)
			Waveform file contents:
				- 1byte boolean: result
				- 4byte unsigned int: EventID
				- 1byte unsigned int: board channel
				- 8byte unsigned int: timestamp
				- 8byte unsigned int: request time
				- 2byte unsigned int: wavesource
				- 4byte int: Waveform Length (N)
				- N 2byte short values that define the waveform itself
		// */
		//determining the size of the header by reading it in line by line:
		finWf.seekg(0,finWf.beg); //starting from the beginning
		
		// getting the pre-header info
		UInt_t version, headerLength_fromFile;
		finWf.read(reinterpret_cast<Char_t*>(&version),sizeof (UInt_t));
		finWf.read(reinterpret_cast<Char_t*>(&headerLength_fromFile),sizeof (UInt_t));
		
		Int_t headerLength = headerLength_fromFile;
		
		//finding the 1st waveform needing to be read
		finWf.seekg(headerLength+8,finWf.beg); //adding 8 to account for the first 8 bytes
		
		Long64_t currLoc = finWf.tellg();//LDM3 troubleshooting
		Long64_t readLoc{};
		Long64_t sizeB4wfLen = sizeof(Bool_t) + sizeof(UInt_t) + sizeof(UChar_t) 
			+ 2*sizeof(ULong_t) + sizeof(UShort_t); 
				
		if (wfNum < 0){
			wfNum=0; //if usr didn't provide wfNum, we automatically look at the first nonzero wf
		}
		wf_Num = wfNum;
		if( (numEnt-wfNum)<500 || wfNum>numEnt ) wfNum = numEnt - 500; //to make sure we don't read past the last wf
		
		//stepping through the data to find the 1st wf needing to be read (specified by usr)
		ULong64_t currWfNum = -1; //is increased by 1 in the next line...
		do{
			currWfNum++; //increment if we're past the first wf
			finWf.seekg(sizeB4wfLen, finWf.cur); //skip to the wfLen
			readLoc = finWf.tellg(); //saving the current location to go back to later
			finWf.read(reinterpret_cast<Char_t*>(&wfLen), sizeof (UInt_t)); //read in the wfLen
			finWf.seekg( wfLen*sizeof(UShort_t), finWf.cur);
		}while (currWfNum!=wfNum);

		//now that we're at the first waveform we want to look at, let's load in this one + the next 500
		//first, determine the max wfLen before initializing the wfArr
		wfLenArr = new UInt_t[500];
		wfLenArr[0] = wfLen;
		//skipping this wf for now and going to the beg of the next entry
		for (Int_t i=1; i<500; i++){
			//skipping to the wfLen
			finWf.seekg(sizeB4wfLen, finWf.cur);
			//read in the wfLen for this one
			finWf.read(reinterpret_cast<Char_t*>(&wfLenArr[i]), sizeof(UInt_t));
			//skipping the waveform for now
			finWf.seekg( wfLenArr[i]*sizeof(UShort_t),finWf.cur);
		}
		//now that we know all of the wfLens, let's determine the largest one
		maxWfLen = wfLenArr[0];
		for (Int_t i=1; i<500; i++)
			if (wfLenArr[i]>maxWfLen) maxWfLen=wfLenArr[i];
		
		//now we can initialize a rectangular array with these sizes
		for (Int_t i{}; i<500; i++)	
			wfArr[i] = {new UShort_t[maxWfLen]};
		
		//with these arrays, we can now fill them with the wf contents
		//need to go back to that original read location
		finWf.seekg(readLoc,finWf.beg);
		Int_t tempVal; //LDM3 troubleshooting
		for (Int_t i{}; i<500; i++){
			finWf.read(reinterpret_cast<Char_t*>(&tempVal), sizeof(UInt_t));
			//reading in the wf contents now
			for (Int_t j{}; j<wfLenArr[i]; j++){
				finWf.read(reinterpret_cast<Char_t*>(&wfArr[i][j]), sizeof (UShort_t));
				//if this wf is shorter than the max size, then fill the empty slots with 0
				if ( wfLenArr[i]<maxWfLen && (j+1)==wfLenArr[i] ){
					for (Int_t k=wfLenArr[i]; k<maxWfLen; k++)
							wfArr[i][k]=0;
				}
				if (wfArr[i][j] < 10000) wfArr[i][j]+=16383;
			}
			//skipping past the pre-wfLen info...
			finWf.seekg(sizeB4wfLen, finWf.cur);
		}
	}
	else
		errorFlag = {1};

	finWf.close();
}

//----------------------------------------------------------------------------------------

//LDM3
Replay::waveformData::~waveformData(){
	if (wfArr || wfLenArr){ 
		delete [] wfLenArr;
		for (Int_t i{}; i<500; i++)
			delete [] wfArr[i];
	}
		
}

//----------------------------------------------------------------------------------------

//LDM3
void Replay::paramsData::readData(){
	std::ifstream infile{fileName};
	std::string line;
	std::string startPhrase{"Board*8+Channel, Pixel"};
	Int_t bcNum{};
	UChar_t pixNum{};
		
	
	if (infile.good()){
		//LDM3 searching the text file for this start phrase
		while( getline(infile,line) && line.find(startPhrase,0)==std::string::npos ){} 
		
		//now to fill the bdChPixel array with the info starting from the place we found
		getline(infile,line,',');
		while (line.length() < 10){
			bcNum = Int_t( std::stoi(line) );
			getline(infile,line,'\n');	
			pixNum = UChar_t( std::stoi(line) );
			bcPixArr[bcNum] = pixNum;
			getline(infile,line,','); //load in the next line
		}
	}
	infile.close();
}

//----------------------------------------------------------------------------------------

Replay::Replay()
{
	//initialize data members
	//LDM3 ... this is all wrong... replacing this mess in Replay::paramsData::readData()... sets
	/*
	bdChPixel[1] = {39};	bdChPixel[2] = {40};	bdChPixel[3] = {41};	bdChPixel[4] = {50};
	bdChPixel[5] = {51};	bdChPixel[6] = {52};	bdChPixel[7] = {53};	bdChPixel[9] = {62};
	bdChPixel[10] = {63};	bdChPixel[11] = {64};	bdChPixel[12] = {65};	bdChPixel[13] = {66};
	bdChPixel[14] = {75};	bdChPixel[15] = {76};	bdChPixel[17] = {77};	bdChPixel[18] = {78};
	bdChPixel[19] = {87};	bdChPixel[20] = {88};	bdChPixel[21] = {89};	bdChPixel[25] = {167};
	bdChPixel[26] = {168};	bdChPixel[27] = {169};	bdChPixel[28] = {178};	bdChPixel[29] = {179};
	bdChPixel[30] = {180};	bdChPixel[31] = {181};	bdChPixel[33] = {190};	bdChPixel[34] = {191};
	bdChPixel[35] = {192};	bdChPixel[36] = {193};	bdChPixel[37] = {194};	bdChPixel[38] = {203};
	bdChPixel[39] = {204};	bdChPixel[41] = {205};	bdChPixel[42] = {206};	bdChPixel[43] = {215};
	bdChPixel[44] = {216};	bdChPixel[45] = {217};
	// */
}

//----------------------------------------------------------------------------------------

Replay::~Replay()
{
	//cleanup
	if(partIndex[0])
	{
		delete[] partIndex[0];
		delete[] partIndex[1];
	}
	if(spectMap[0])
		for(Char_t i{}; i < 3; ++i)
		{
			for(UChar_t j{}; j < 150; ++j)
			{
				for(Char_t k{}; k < 40; ++k)
					delete[] spectMap[i][j][k];
				delete[] spectMap[i][j];
				delete[] spectMapProj[i][j];
			}
			delete[] spectMap[i];
			delete[] spectMapProj[i];
		}
	for(Short_t i{}; i < hist.size(); ++i)
		for(Short_t j{}; j < hist[i].size(); ++j)
			delete hist[i][j];
	for(Short_t i{}; i < funct.size(); ++i)
		for(Short_t j{}; j < funct[i].size(); ++j)
			delete funct[i][j];
	for(Short_t i{1}; i < style.size(); ++i)
		delete style[i];
	for(Short_t i{}; i < text.size(); ++i)
		delete text[i];
	if(gui)
		delete gui;
}

//----------------------------------------------------------------------------------------

void Replay::updateFileInfo()
{
	std::string fileName{};
	std::ifstream fIn{};

	for(Int_t i{}, j{}; i < 10000 && !j; ++i){
		fileName = {tD.dirName + "Run" + std::to_string(i) + "_0.trigg"}; 
		fIn.open(fileName);

		if(~fIn.rdstate() & std::ifstream::failbit){
			for(Long64_t k{1}; !j; ++k){
				fIn.close();
				fileName = {tD.dirName + "Run" + std::to_string(i + k) + "_0.trigg"};
				fIn.open(fileName);

				if(fIn.rdstate() & std::ifstream::failbit){
					runNumLast = {i + k - 1};
					j = {1};
				}
			}
		}
	}
}

//----------------------------------------------------------------------------------------

UChar_t Replay::readTrigFile(Long64_t runNumTemp)
{
	Long64_t rNTemp = {runNum};

	if(runNumTemp != -1)
		runNum = {runNumTemp};
	else
		runNum = {runNumLast};

	tD.fileName = {tD.dirName + "Run" + std::to_string(runNum) + "_0.trigg"};

	//delete existing trig-file data if applicable and read from new trig-file
	tD.readData();
	
	if(reportError() & 1 && runNumTemp != -1)
	{
		runNum = {rNTemp};
		tD.fileName = {tD.dirName + "Run" + std::to_string(runNum) + "_0.trigg"};
	}

	//return errorFlag so that one may easily check its state
	return errorFlag;
}

//----------------------------------------------------------------------------------------

//LDM3: reading the waveform file here
UChar_t Replay::readWfFile(Long64_t runNumTemp)
{
	Long64_t rNTemp = {runNum};

	if(runNumTemp != -1)
		runNum = {runNumTemp};
	else
		runNum = {runNumLast};
	

	wfD.fileName = {wfD.dirName + "Run" + std::to_string(runNum) + "_0.singl"};

	//delete existing trig-file data if applicable and read from new trig-file
	//Long64_t wfNum = 0; //LDM3 troubleshooting... hard coding this for now
	wfD.readData(tD.numEnt);
	
	if(reportError() & 1 && runNumTemp != -1)
	{
		runNum = {rNTemp};
		wfD.fileName = {wfD.dirName + "Run" + std::to_string(runNum) + "_0.singl"};//LDM3
	}
	
	//return errorFlag so that one may easily check its state
	return errorFlag;
}

//----------------------------------------------------------------------------------------

//LDM3: reading the params file here
UChar_t Replay::readParamsFile(Long64_t runNumTemp)
{
	Long64_t rNTemp = {runNum};

	if(runNumTemp != -1)
		runNum = {runNumTemp};
	else
		runNum = {runNumLast};
	

	paramsD.fileName = {paramsD.dirName + "Run" + std::to_string(runNum) + "_0.param"};

	paramsD.readData();
	
	//setting the correct board/channel to pixel number
	for (Int_t i{}; i<256; i++)
		setPixelVal(i,paramsD.bcPixArr[i]);
		
	if(reportError() & 1 && runNumTemp != -1)
	{
		runNum = {rNTemp};
		paramsD.fileName = {paramsD.dirName + "Run" + std::to_string(runNum) + "_0.param"};//LDM3
	}
	
	//return errorFlag so that one may easily check its state
	return errorFlag;
}

//----------------------------------------------------------------------------------------

UChar_t Replay::readSpectMap(std::string fileName = {})
{
	if(!spectMap[0])
	{
		std::ifstream finSpect{fileName, std::ifstream::ate | std::ifstream::binary};

		if(finSpect.good() && finSpect.tellg() == 1152000)
		{
			for(Char_t i{}; i < 3; ++i)
			{
				spectMap[i] = {new Double_t**[150]};
				spectMapProj[i] = {new Double_t*[150]};
				for(UChar_t j{}; j < 150; ++j)
				{
				  spectMap[i][j] = {new Double_t*[40]};
					spectMapProj[i][j] = {new Double_t[8]};
				  for(Char_t k{}; k < 40; ++k)
				    spectMap[i][j][k] = {new Double_t[8]{}};
				}
			}

			finSpect.seekg(0, finSpect.beg);
		  for(UChar_t i{}; i < 150; ++i)
			{
		    for(Char_t j{}; j < 40; ++j)
		      for(Char_t k{}; k < 8; ++k)
		      {
		        finSpect.read(reinterpret_cast<Char_t*>(&spectMap[0][i][j][k]), sizeof (Double_t));
		        finSpect.read(reinterpret_cast<Char_t*>(&spectMap[1][i][j][k]), sizeof (Double_t));
		        finSpect.read(reinterpret_cast<Char_t*>(&spectMap[2][i][j][k]), sizeof (Double_t));
		      }

				for(Char_t j{}; j < 40; ++j)
				  for(Char_t k{}; k < 8; ++k)
				  {
				    spectMapProj[0][i][k] += spectMap[2][i][j][k]*spectMap[0][i][j][k];
				    spectMapProj[1][i][k] += spectMap[2][i][j][k]*spectMap[2][i][j][k]
						*spectMap[1][i][j][k]*spectMap[1][i][j][k];
				    spectMapProj[2][i][k] += spectMap[2][i][j][k];
				  }

			  for(Char_t j{}; j < 8; ++j)
			    if(spectMapProj[2][i][j])
			    {
			      spectMapProj[0][i][j] /= spectMapProj[2][i][j];
			      spectMapProj[1][i][j] = sqrt(spectMapProj[1][i][j])/spectMapProj[2][i][j];
			    }
			}
		}
		else
		  errorFlag |= 2;

		return errorFlag;
	}
	else
		return 0x80;
}

//----------------------------------------------------------------------------------------

Char_t Replay::pixRingSelect(Long64_t x)
{
	Char_t pixRing;
  UChar_t pixel{bdChPixel[8*pD.bdNum[0][x] + pD.chNum[0][x]]};

  if(pixel == 64)
    pixRing = {0};
  else if(pixel == 51 || pixel == 52 || pixel == 63 || pixel == 65 || pixel == 76 || pixel == 77)
    pixRing = {1};
  else if((pixel > 38 && pixel < 42) || pixel == 50 || pixel == 53 || pixel == 62 || pixel == 66
          || pixel == 75 || pixel == 78 || (pixel > 86 && pixel < 90))
    pixRing = {2};
  else if((pixel > 27 && pixel < 32) || pixel == 38 || pixel == 42 || pixel == 49 || pixel == 54
          || pixel == 61 || pixel == 67 || pixel == 74 || pixel == 79 || pixel == 86 || pixel == 90
          || (pixel > 96 && pixel < 101))
    pixRing = {3};
  else if((pixel > 17 && pixel < 23) || pixel == 27 || pixel == 32 || pixel == 37 || pixel == 43
          || pixel == 48 || pixel == 55 || pixel == 60 || pixel == 68 || pixel == 73 || pixel == 80
          || pixel == 85 || pixel == 91 || pixel == 96 || pixel == 101 || (pixel > 105
          && pixel < 111))
    pixRing = {4};
  else if((pixel > 8 && pixel < 15) || pixel == 17 || pixel == 23 || pixel == 26 || pixel == 33
          || pixel == 36 || pixel == 44 || pixel == 47 || pixel == 56 || pixel == 59 || pixel == 69
          || pixel == 72 || pixel == 81 || pixel == 84 || pixel == 92 || pixel == 95 || pixel == 102
          || pixel == 105 || pixel == 111 || (pixel > 113 && pixel < 120))
    pixRing = {5};
  else if((pixel > 0 && pixel < 9) || pixel == 15 || pixel == 16 || pixel == 24 || pixel == 25
          || pixel == 34 || pixel == 35 || pixel == 45 || pixel == 46 || pixel == 57 || pixel == 58
          || pixel == 70 || pixel == 71 || pixel == 82 || pixel == 83 || pixel == 93 || pixel == 94
          || pixel == 103 || pixel == 104 || pixel == 112 || pixel == 113 || (pixel > 119
          && pixel < 128))
    pixRing = {6};
  else
    pixRing = {7};

	return pixRing;
}

//----------------------------------------------------------------------------------------

void Replay::sigCompile()
{
	Long64_t tempCnt{};
	UShort_t tempAdc{};

	for(Long64_t i{}; i < tD.numEnt; ++i)
		if(partIndex[0][i] == 1)
			++tempCnt;
	
	pD.setData(tempCnt);

	//assumes only 1 proton signal per particle pair
	for(Long64_t i{}, j{}, k{}; j < pD.numEnt; ++i)
		if(partIndex[0][partIndex[1][i]] == 1)
		{
			pD.bdNum[0][j] = {tD.bdNum[partIndex[1][i]]};
			pD.chNum[0][j] = {tD.chNum[partIndex[1][i]]};
			pD.timeReq[0][j] = {tD.timeReq[partIndex[1][i]]};
			pD.adcVal[0][j] = {tD.adcVal[partIndex[1][i]]};
			pD.partNum[0][j] = {1};
			pD.bdNum[1][j] = {tD.bdNum[partIndex[1][i-1]]};
			pD.chNum[1][j] = {tD.chNum[partIndex[1][i-1]]};
			pD.timeReq[1][j] = {tD.timeReq[partIndex[1][i-1]]};
			pD.adcVal[1][j] = {tempAdc};
			pD.partNum[1][j] = {static_cast<Short_t>(k)};
			tempAdc = {0};
			++j;
			k = {};
		}
		else if(partIndex[0][partIndex[1][i]])
		{
			tempAdc += tD.adcVal[partIndex[1][i]];
			++k;
		}
}

//---------------------------------------------------------------------------------------

UChar_t Replay::findCosThTop(TH1D* histCorPar, Double_t domain[2])
{
	Int_t numBins{histCorPar->GetNbinsX() + 1};
	Double_t max{};
	Int_t maxBin{};
	Char_t flag{};

	for(Int_t i{1}; i < numBins; ++i){
	    if(histCorPar->GetBinContent(i) > max)
	    {
	    	max = {histCorPar->GetBinContent(i)};
	      	maxBin = {i};
	    }
	}
	if(maxBin <= numBins/2 + 1)
	    max = {histCorPar->GetBinContent(maxBin + numBins/20)};
	else if(maxBin < numBins - numBins/20)
	    max = {histCorPar->GetBinContent(maxBin - numBins/20)};
	else
		max = {-1};
	if(max != -1){
		for(Int_t i{1}; i < numBins; ++i){
			if(flag < 3){
				if(histCorPar->GetBinContent(i) > 2.*max/3.){
     	  			if(flag == 2){
     	    			domain[0] = {histCorPar->GetBinLowEdge(i)};
						i += 2;
					}
     	  			++flag;
     	 		}
     	 		else
     	  			flag = {};
			}
    		else{
    			if(histCorPar->GetBinContent(i) < 2*max/3){
   	  				if(flag == 5){
   	  					domain[1] = {histCorPar->GetBinLowEdge(i - 4)};
						i = {numBins};
					}
					++flag;
				}
				else
					flag = {3};
			}
		}
  		if(flag != 6)
			return 2;
	}
	else
		return 1;

	return 0;
}

//---------------------------------------------------------------------------------------

void Replay::compTotCorPar()
{
	for(Short_t i{}; i < 39; ++i)
	{
		valCorParTot[i][0] = {};
		valCorParTot[i][1] = {};
	}
	for(Long64_t i{}; i < vCPTFlag.size(); ++i){
		for(Short_t j{}; j < 39; ++j)
		{
			valCorParTot[j][0] += corParTot[j][0][i]/corParTot[j][1][i]/corParTot[j][1][i];
			valCorParTot[j][1] += 1./corParTot[j][1][i]/corParTot[j][1][i];
		}
	}
	
	for(Short_t i{}; i < 39; ++i)
	{
		valCorParTot[i][0] /= valCorParTot[i][1];
		valCorParTot[i][1] = {sqrt(1./valCorParTot[i][1])};
	}
	
}

//---------------------------------------------------------------------------------------

void Replay::fillStats(Char_t bdNum, Char_t chNum)
{
	std::string stemp{};
	Double_t totalA[4]{};
	Long64_t numEntPix[4]{};

  if(bdNum != 6)
	{
		for(Long64_t i{0}; i < tD.numEnt; ++i)
		{
			if(bdNum == tD.bdNum[i] && chNum == tD.chNum[i])
				++numEntPix[0];
	
			if(!partIndex[0][i] && bdNum == tD.bdNum[i] && chNum == tD.chNum[i])
				++numEntPix[3];
		}

		for(Long64_t i{0}; i < pD.numEnt; ++i)
		{
			if(bdNum == pD.bdNum[0][i] && chNum == pD.chNum[0][i])
				++numEntPix[1];

			if(bdNum == pD.bdNum[1][i] && chNum == pD.chNum[1][i])
				++numEntPix[2];
		}
	}
	else
	{
		numEntPix[0] = {tD.numEnt};
		numEntPix[1] = {pD.numEnt};
		numEntPix[2] = {pD.numEnt};
	
		for(Long64_t i{0}; i < tD.numEnt; ++i)
			if(!partIndex[0][i])
				++numEntPix[3];
	}

	if(!runFlag)
		text.push_back(new TPaveText{.02, .05, .48, .95, "nb"});

	text[0]->Clear();
	stemp = {"Number of Triggers: " + std::to_string(numEntPix[0])};
	text[0]->AddText(stemp.c_str());
	stemp = {"Number of Protons: " + std::to_string(numEntPix[1])};
	text[0]->AddText(stemp.c_str());
	stemp = {"Number of Electrons: " + std::to_string(numEntPix[2])};
	text[0]->AddText(stemp.c_str());
	stemp = {"Number of Unidentified: " + std::to_string(numEntPix[3])};
	text[0]->AddText(stemp.c_str());

	for(Char_t i{5}; i < 30; ++i)
	{
		totalA[0] += valueCorPar[i][0]/valueCorPar[i][1]/valueCorPar[i][1];
		totalA[1] += 1./valueCorPar[i][1]/valueCorPar[i][1];
		totalA[2] += valCorParTot[i][0]/valCorParTot[i][1]/valCorParTot[i][1];
		totalA[3] += 1./valCorParTot[i][1]/valCorParTot[i][1];
	}

	totalA[0] /= totalA[1];
	totalA[1] = {sqrt(1./totalA[1])};
	totalA[2] /= totalA[3];
	totalA[3] = {sqrt(1./totalA[3])};

	if(!runFlag)
		text.push_back(new TPaveText{.52, .05, .98, .95, "nb"});

	text[1]->Clear();
	stemp = {"Current #it{a}: " + std::to_string(totalA[0]) + " #pm " + std::to_string(totalA[1])};
	text[1]->AddText(stemp.c_str());
	stemp = {"Total #it{a}: " + std::to_string(totalA[2]) + " #pm " + std::to_string(totalA[3])};
	text[1]->AddText(stemp.c_str());

  gui->selCanvas(0, 1, 0);
	text[0]->Draw();
	text[1]->Draw();
	gui->updateCanvas(0, 1);
}

//---------------------------------------------------------------------------------------

void Replay::partIdent()
{
	//declare and define variables
	ULong64_t protTime;
	ULong64_t protID;
	Char_t tempC{0};

	//delete existing particle index if applicable, and instantiate new one
	if(partIndex[0])
	{
		delete[] partIndex[0];
		delete[] partIndex[1];
	}
	partIndex[0] = {new ULong64_t[tD.numEnt]{}};
	partIndex[1] = {new ULong64_t[tD.numEnt]{}};

	for(ULong64_t i{0}, j{0}; i < tD.numEnt; ++i){
		//setting energy window for electron: //LDM3
		if(tD.adcVal[i] > 10./calib && tD.adcVal[i] < 26./calib && 8*tD.bdNum[i] + tD.chNum[i] < 24)
		{
		 	protTime = {tD.timeReq[i]};
		 	protID = {static_cast<ULong64_t>(1e14)*tD.adcVal[i] 
		 		+ static_cast<ULong64_t>(1e11)*(8*tD.bdNum[i] + tD.chNum[i])+ protTime%static_cast<ULong64_t>(1e11)};
			tempC = {0};
			
			//looking for coincident protons (?) //LDM3
		 	for(ULong64_t k{1}; k <= i; ++k){
		 		//setting coincidence window: //LDM3
				if(tD.timeReq[i - k] + 10000 > protTime && tD.timeReq[i - k] + 3000 < protTime)
				{
				//setting energy window for this 2nd particle && that we haven't checked this particle already //LDM3
				 	if(tD.adcVal[i - k] > 10./calib && partIndex[0][i - k] == 0)
				 	{
				 		partIndex[0][i - k] = {protID};
						partIndex[1][j++] = {i - k};
				    	tempC = {1};
				  	}
				}
				//as soon as your particle doesn't fall within the time window,
				// exit the proton-hunt for-loop and move on to the next potential proton //LDM3
				else
					k = {i + 1};
			}
			
			//if tempC=1, then save this as a legit proton-electron coincidence
			if(tempC)
			{
			    partIndex[0][i] = {1}; //does 1 mean it's a proton? //LDM3
			    partIndex[1][j++] = {i};
			}
		}
	}

	sigCompile();
}

//----------------------------------------------------------------------------------------

void Replay::computeProtData()
{
	Double_t tempTime;
	Short_t tempPix;
	for(Long64_t i{}; i < pD.numEnt; ++i)
	{
		tempTime = {(pD.timeReq[0][i] - pD.timeReq[1][i])/250.};
		tempPix = {pixRingSelect(i)};
		
		for(UChar_t j{}; j < 150; ++j)
			if(1/tempTime/tempTime >= .000042*j && 1/tempTime/tempTime < .000042*(j + 1))
			{
			  pD.protMom[i] = {massProt*spectMapProj[0][j][tempPix]/sqrt(1e-12*spdLight*spdLight
				*tempTime*tempTime + spectMapProj[0][j][tempPix]*spectMapProj[0][j][tempPix])};
				
			  pD.cosTheta[i] = {(pD.protMom[i]*pD.protMom[i] - calib*calib*pD.adcVal[1][i]
				*pD.adcVal[1][i] - 2.*calib*pD.adcVal[1][i]*massElec - (qValue - calib
				*pD.adcVal[1][i] - sqrt(massProt*massProt + pD.protMom[i]*pD.protMom[i])
				+ massProt)*(qValue - calib*pD.adcVal[1][i] - sqrt(massProt*massProt
				+ pD.protMom[i]*pD.protMom[i]) + massProt))/(2.*sqrt(calib*calib*pD.adcVal[1][i]
				*pD.adcVal[1][i] + 2.*massElec*calib*pD.adcVal[1][i])*(qValue
				- calib*pD.adcVal[1][i] - sqrt(massProt*massProt + pD.protMom[i]*pD.protMom[i])
				+ massProt))};

				//LDM3 to prevent crashing when reading in the manitoba test data
				if (pD.protMom[i]==0 || pD.cosTheta[i]>1 || pD.cosTheta[i]<-1) 
					pD.flag = true;
			}
	}
	if (pD.numEnt == 0) pD.flag = true; //LDM3 to check later and prevent crashing
}

//---------------------------------------------------------------------------------------

void Replay::extractCorPar()
{
	TH1D* histCorPar;
	Double_t domain[2];
	Double_t midPt;
	std::string stemp[2]{"corParHist ", "corParFit "};
	UChar_t topFlag;
	Char_t corTotFlag{1};

	if(linFitCorPar[0])
		for(Char_t i{}; i < 39; ++i)
			delete linFitCorPar[i];

	for(Long64_t i{}; i < vCPTFlag.size(); ++i)
		if(vCPTFlag[i] == runNum)
			corTotFlag = {};

	if(corTotFlag)
		vCPTFlag.push_back(runNum);
		
	
  for(Int_t i{}; i < 39; ++i)
  {
		stemp[0].pop_back();
		stemp[0].push_back(i);
		histCorPar = {new TH1D{stemp[0].c_str(), "", 200, -1.2, 1.2}};

		for(Long64_t j{}; j < pD.numEnt; ++j)
			if(calib*pD.adcVal[1][j] >= 20.*i && calib*pD.adcVal[1][j] < 20.*i + 20)
				histCorPar->Fill(pD.cosTheta[j]);

		topFlag = {findCosThTop(histCorPar, domain)};
		
    if(!topFlag)
    {
      midPt = (domain[0] + (domain[1] - domain[0])/2);
			domain[0] += (2.5+(i-5.)/48.)*(domain[1] - domain[0])/10.;
			domain[1] -= (3.+(i-5.)/12.)*(domain[1] - domain[0])/(10.-2.5-(i-5.)/48.);
      //domain[0] += .2*(domain[1] - domain[0]);
      //domain[1] -= .25*(domain[1] - domain[0]);

			stemp[1].pop_back();
			stemp[1].push_back(i);
    	linFitCorPar[i] = {new TF1{stemp[1].c_str(), "[0]*x+[1]", domain[0], domain[1]}};
    	linFitCorPar[i]->SetParameters(0.,1.);
    	histCorPar->Fit(stemp[1].c_str(), "QRN");

    	midPt = {linFitCorPar[i]->GetParameter(0)*midPt
    	+ linFitCorPar[i]->GetParameter(1)};

			valueCorPar[i][0] = {linFitCorPar[i]->GetParameter(0)/midPt
    	/sqrt(1. - massElec*massElec/(massElec + 20.*i + 10.)/(massElec + 20.*i + 10.))};
			valueCorPar[i][1] = {linFitCorPar[i]->GetParError(0)/midPt
    	/sqrt(1. - massElec*massElec/(massElec + 20.*i + 10.)/(massElec + 20.*i + 10.))};

			if(corTotFlag)
			{
				corParTot[i][0].push_back(valueCorPar[i][0]);
				corParTot[i][1].push_back(valueCorPar[i][1]);
			}
		}
		else
		{
			valueCorPar[i][0] = {1000.};
			valueCorPar[i][1] = {1000.};
		}

		delete histCorPar;
  }
	if(corTotFlag && !pD.flag){ //LDM3 added the 2nd conditional to prevent crashing
		compTotCorPar();
	}
}

//----------------------------------------------------------------------------------------

void Replay::createGUI()
{
	//instantiate ReplayGUI object, pass a pointer of the current instance of Replay, and create tabs
  gui = {new ReplayGUI{}};
  gui->accessData(this);
  gui->createTabs();

	//initialize vectors for histograms and functions
	hist.resize(15);
	hist2D.resize(15); //LDM3 for the waveform viewer
	funct.resize(15);

	//create instances of TStyle and set style attributes
	style.push_back(gStyle);
	style[0]->SetOptStat(0);
	style[0]->SetTitleFontSize(.04);
  style[0]->SetTitleOffset(1.25, "x");
  style[0]->SetTitleOffset(1.15, "y");
	style[0]->SetTitleOffset(1.15, "z");
	
	style.push_back(new TStyle{*style[0]});
  style[1]->SetTitleOffset(.65, "y");
	style[1]->SetTitleOffset(.7, "z");
	
	style.push_back(new TStyle{*style[0]});
  style[2]->SetTitleOffset(1.1, "y");
	style[2]->SetTitleOffset(.7, "z");
	style[2]->SetLabelSize(.02, "z");
	
	style.push_back(new TStyle{*style[0]});
  style[3]->SetTitleOffset(1.3, "x");
	style[3]->SetTitleOffset(1.5, "y");
	style.push_back(new TStyle{*style[0]});
	style[4]->SetAxisColor(0, "xy");
	style[4]->SetLabelSize(0., "xy");
	style[4]->SetTickLength(0., "xy");
	style[4]->SetLabelSize(.02, "z");
	style[4]->SetTitleSize(.02, "z");
	style[4]->SetPalette(kSunset);
	
	style.push_back(new TStyle{*style[4]});
	style[5]->SetNameTitle("mainCS", "main canvas style");
	style[5]->SetTitleOffset(1.5, "z");
}

//----------------------------------------------------------------------------------------

void Replay::fillGUI()
{
	//fill tabs
	plotMain();
	plotAllTrig0(6, 0);
	plotProton0(6, 0);
	plotProton1(6, 0);
	plotElectron0(6, 0);
	plotElectron1(6, 0);
	plotOther0(6, 0);
	plotOther1(6, 0);
	plotPair0();
	plotPair1();
	plotPair2();
	plotPair3();
	plotAExtraction();
	plotWaveforms(); //LDM3
	
	if(!runFlag)
		runFlag = {1};
}

//----------------------------------------------------------------------------------------

void Replay::plotMain()
{
	//instantiate and initialize variables
	const Double_t hexFact{sqrt(3.)/2.};
	const Double_t hexSide{.042};
	TH2Poly* histTemp[2];
	Double_t binCoord[2][6];
	Long64_t normCount[2]{};
	Double_t minTemp[]{100., 100.};

	//delete existing histograms if applicable, create new histograms, and fill histograms
	if(hist[0].size())
	{
    delete hist[0][0];
    delete hist[0][1];
		hist[0].clear();
	}
	style[5]->cd();
	hist[0].push_back(new TH2Poly{"pixMapTrigUHist", "Trigger Yield:  Upper Detector", 0., 1., 0., 1.});
	hist[0].push_back(new TH2Poly{"pixMapTrigDHist", "Trigger Yield:  Lower Detector", 0., 1., 0., 1.});
	histTemp[0] = {dynamic_cast<TH2Poly*>(hist[0][0])};
	histTemp[1] = {dynamic_cast<TH2Poly*>(hist[0][1])};

	for(Char_t i{}, k{}; i < 13; ++i)
		for(Char_t j{}; j < (i < 7 ? i + 7 : 19 - i); ++j, ++k)
		{
			binCoord[0][0] = {1.5*hexSide*i + .101};
			binCoord[1][0] = {hexSide*hexFact*(i < 7 ? -i : i - 12) + 2.*hexSide*hexFact*j + .246};
			binCoord[0][1] = {binCoord[0][0] + hexSide};
			binCoord[1][1] = {binCoord[1][0]};
			binCoord[0][2] = {binCoord[0][1] + hexSide/2.};
			binCoord[1][2] = {binCoord[1][1] + hexSide*hexFact};
			binCoord[0][3] = {binCoord[0][1]};
			binCoord[1][3] = {binCoord[1][2] + hexSide*hexFact};
			binCoord[0][4] = {binCoord[0][0]};
			binCoord[1][4] = {binCoord[1][3]};
			binCoord[0][5] = {binCoord[0][4] - hexSide/2.};
			binCoord[1][5] = {binCoord[1][2]};
			histTemp[0]->AddBin(6, binCoord[0], binCoord[1]);
			histTemp[1]->AddBin(6, binCoord[0], binCoord[1]);
			binCenter[0][k + (i < 7 ? i + 7 : 19 - i) - 2*j - 1] = {binCoord[0][5] + hexSide};
			binCenter[1][k + (i < 7 ? i + 7 : 19 - i) - 2*j - 1] = {binCoord[1][5]};
		}

	for(Long64_t i{}; i < tD.numEnt; ++i)
		if(bdChPixel[8*tD.bdNum[i] + tD.chNum[i]])
		{
			if(bdChPixel[8*tD.bdNum[i] + tD.chNum[i]] < 128)
			{
				histTemp[0]->Fill(binCenter[0][bdChPixel[8*tD.bdNum[i] + tD.chNum[i]] - 1],
				binCenter[1][bdChPixel[8*tD.bdNum[i] + tD.chNum[i]] - 1]);
				++normCount[0];
			}
			else
			{
				histTemp[1]->Fill(binCenter[0][bdChPixel[8*tD.bdNum[i] + tD.chNum[i]] - 129],
				binCenter[1][bdChPixel[8*tD.bdNum[i] + tD.chNum[i]] - 129]);
				++normCount[1];
			}
		}

	if(normCount[0])
	{
		histTemp[0]->Scale(100./normCount[0]);
		for(UChar_t i{1}; i < 128; ++i)
			if(histTemp[0]->GetBinContent(i) && histTemp[0]->GetBinContent(i) < minTemp[0])
				minTemp[0] = {histTemp[0]->GetBinContent(i)};
	}

	if(normCount[1])
	{
		histTemp[1]->Scale(100./normCount[1]);
		for(UChar_t i{1}; i < 128; ++i)
			if(histTemp[1]->GetBinContent(i) && histTemp[1]->GetBinContent(i) < minTemp[1])
				minTemp[1] = {histTemp[1]->GetBinContent(i)};
	}

	//change to plotMain canvases and draw histograms
	hist[0][0]->SetZTitle("Yield (%)");
	hist[0][1]->SetZTitle("Yield (%)");
  gui->selCanvas(0, 0, 1);
	gPad->SetFrameLineColor(0);
	gPad->SetMargin(0., .15, .05, .2);
	gPad->SetLogz();
	hist[0][0]->SetMinimum(pow(10, floor(log10(minTemp[0]))));
	hist[0][0]->Draw("colz");
  gui->selCanvas(0, 0, 2);
	gPad->SetFrameLineColor(0);
	gPad->SetMargin(0., .15, .05, .2);
	gPad->SetLogz();
	hist[0][1]->SetMinimum(pow(10, floor(log10(minTemp[1]))));
	hist[0][1]->Draw("colz");
	gui->updateCanvas(0, 0);

	fillStats(6, 0);
}

//----------------------------------------------------------------------------------------

void Replay::plotAllTrig0(Char_t bdNum, Char_t chNum)
{
	//instantiate and initialize variables
	TH2D* histTemp;
	std::string stemp[3]{};
	Long64_t normCount{};

	//delete existing histograms if applicable, create new histograms, and fill, depending upon pixel
	if(hist[1].size())
	{
	  delete hist[1][0];
		delete hist[1][1];
		delete hist[1][2];
		hist[1].clear();
	}
  if(bdNum != 6)
	{
		stemp[0] = {"Trigger Timestamp-Spectrum: Board " + std::to_string(bdNum) + ", Channel "
		+ std::to_string(chNum)};
		stemp[1] = {"Trigger Energy-Spectrum: Board " + std::to_string(bdNum) + ", Channel "
		+ std::to_string(chNum)};
		stemp[2] = {"Trigger 2D Energy-Timestamp-Spectrum: Board " + std::to_string(bdNum)
		+ ", Channel " + std::to_string(chNum)};
		style[0]->cd();
	  hist[1].push_back(new TH1D{"tTimeHist", stemp[0].c_str(), 500, 4.*tD.timeReq[0],
		4.*tD.timeReq[tD.numEnt-1]});
	  hist[1].push_back(new TH1D{"tEnergyHist", stemp[1].c_str(), 300, 0., 900.});
		style[1]->cd();
	  hist[1].push_back(new TH2D{"tEnergyTimeHist", stemp[2].c_str(), 200, 4.*tD.timeReq[0],
		4.*tD.timeReq[tD.numEnt - 1], 300, 0., 900.});
		histTemp = {dynamic_cast<TH2D*>(hist[1][2])};

		for(Long64_t i{0}; i < tD.numEnt; ++i)
			if(bdNum == tD.bdNum[i] && chNum == tD.chNum[i])
			{
				hist[1][0]->Fill(4.*tD.timeReq[i]);
				hist[1][1]->Fill(calib*tD.adcVal[i]);
				histTemp->Fill(4.*tD.timeReq[i], calib*tD.adcVal[i]);
				++normCount;
			}

		if(normCount)
		{
			for(Short_t i{}; i < 500; ++i)
				hist[1][0]->SetBinContent(i, 100.*hist[1][0]->GetBinContent(i)/normCount);

			for(Short_t i{}; i < 300; ++i)
				hist[1][1]->SetBinContent(i, 100.*hist[1][1]->GetBinContent(i)/normCount);

			for(UChar_t i{}; i < 200; ++i)
				for(Short_t j{}; j < 300; ++j)
					histTemp->SetBinContent(i, j, 100.*histTemp->GetBinContent(i, j)/normCount);
		}
	}
  else
	{
		style[0]->cd();
	  hist[1].push_back(new TH1D{"tTimeHist", "Trigger Timestamp-Spectrum: All Pixels",
		500, 4.*tD.timeReq[0], 4.*tD.timeReq[tD.numEnt - 1]});
	  hist[1].push_back(new TH1D{"tEnergyHist", "Trigger Energy-Spectrum: All Pixels", 300,
		0., 900.});
		style[1]->cd();
	  hist[1].push_back(new TH2D{"tEnergyTimeHist", "Trigger Energy-Timestamp-Spectrum: All"
		"Pixels", 200, 4.*tD.timeReq[0], 4.*tD.timeReq[tD.numEnt - 1], 300, 0., 900.});
		histTemp = {dynamic_cast<TH2D*>(hist[1][2])};

		for(Long64_t i{0}; i < tD.numEnt; ++i)
		{
			hist[1][0]->Fill(4.*tD.timeReq[i], 1./tD.numEnt);
			hist[1][1]->Fill(calib*tD.adcVal[i], 1./tD.numEnt);
			histTemp->Fill(4.*tD.timeReq[i], calib*tD.adcVal[i], 1./tD.numEnt);
		}
	}

	//change to allTrig0 canvas, set y-axis to log-scale, create axis titles, and draw histogram
  gui->selCanvas(1, 0, 1);
	gPad->SetLogy();
  hist[1][0]->GetXaxis()->SetTitle("Trigger Timestamp (ns)");
  hist[1][0]->GetYaxis()->SetTitle("Yield");
  hist[1][0]->Draw();
  gui->selCanvas(1, 0, 2);
	gPad->SetLogy();
  hist[1][1]->GetXaxis()->SetTitle("Trigger Energy (keV)");
  hist[1][1]->GetYaxis()->SetTitle("Yield");
  hist[1][1]->Draw();
	gui->updateCanvas(1, 0);
  gui->selCanvas(1, 1, 0);
	gPad->SetLogz();
  hist[1][2]->GetXaxis()->SetTitle("Trigger Timestamp (ns)");
  hist[1][2]->GetYaxis()->SetTitle("Trigger Energy (keV)");
  hist[1][2]->GetZaxis()->SetTitle("Yield");
  hist[1][2]->Draw("colz");
	gui->updateCanvas(1, 1);
}

//----------------------------------------------------------------------------------------

void Replay::plotProton0(Char_t bdNum, Char_t chNum)
{
	//instantiate and initialize variables
	std::string stemp[2]{};
	const Double_t hexFact{sqrt(3.)/2.};
	const Double_t hexSide{.042};
	TH2Poly* histTemp0[2];
	TH2D* histTemp1;
	Double_t binCoord[2][6];
	Long64_t normCount[3]{};
	Double_t minTemp[]{100., 100.};

	//delete existing histograms if applicable, create new histograms, and fill, depending upon pixel
	if(hist[2].size())
	{
	  delete hist[2][0];
		delete hist[2][1];
		delete hist[2][2];
		delete hist[2][3];
		hist[2].clear();
	}
	style[4]->cd();
	hist[2].push_back(new TH2Poly{"pixMapProtUHist", "Proton Yield:  Upper Detector", 0., 1., 0., 1.});
	hist[2].push_back(new TH2Poly{"pixMapProtDHist", "Proton Yield:  Lower Detector", 0., 1., 0., 1.});
	histTemp0[0] = {dynamic_cast<TH2Poly*>(hist[2][0])};
	histTemp0[1] = {dynamic_cast<TH2Poly*>(hist[2][1])};
	for(Char_t i{0}, k{0}; i < 13; ++i)
		for(Char_t j{0}; j < (i < 7 ? i + 7 : 19 - i); ++j, ++k)
		{
			binCoord[0][0] = {1.5*hexSide*i + .101};
			binCoord[1][0] = {hexSide*hexFact*(i < 7 ? -i : i - 12) + 2.*hexSide*hexFact*j + .246};
			binCoord[0][1] = {binCoord[0][0] + hexSide};
			binCoord[1][1] = {binCoord[1][0]};
			binCoord[0][2] = {binCoord[0][1] + hexSide/2.};
			binCoord[1][2] = {binCoord[1][1] + hexSide*hexFact};
			binCoord[0][3] = {binCoord[0][1]};
			binCoord[1][3] = {binCoord[1][2] + hexSide*hexFact};
			binCoord[0][4] = {binCoord[0][0]};
			binCoord[1][4] = {binCoord[1][3]};
			binCoord[0][5] = {binCoord[0][4] - hexSide/2.};
			binCoord[1][5] = {binCoord[1][2]};
			histTemp0[0]->AddBin(6, binCoord[0], binCoord[1]);
			histTemp0[1]->AddBin(6, binCoord[0], binCoord[1]);
			binCenter[0][k + (i < 7 ? i + 7 : 19 - i) - 2*j - 1] = {binCoord[0][5] + hexSide};
			binCenter[1][k + (i < 7 ? i + 7 : 19 - i) - 2*j - 1] = {binCoord[1][5]};
		}

	for(Long64_t i{0}; i < pD.numEnt; ++i)
		if(bdChPixel[8*pD.bdNum[0][i] + pD.chNum[0][i]] != 0)
		{
			if(bdChPixel[8*pD.bdNum[0][i] + pD.chNum[0][i]] < 128)
			{
				histTemp0[0]->Fill(binCenter[0][bdChPixel[8*pD.bdNum[0][i] + pD.chNum[0][i]] - 1],
				binCenter[1][bdChPixel[8*pD.bdNum[0][i] + pD.chNum[0][i]] - 1]);
				++normCount[0];
			}
			else
			{
				histTemp0[1]->Fill(binCenter[0][bdChPixel[8*pD.bdNum[0][i] + pD.chNum[0][i]] - 129],
				binCenter[1][bdChPixel[8*pD.bdNum[0][i] + pD.chNum[0][i]] - 129]);
				++normCount[1];
			}
		}

	if(normCount[0])
	{
		histTemp0[0]->Scale(100./normCount[0]);
		for(UChar_t i{1}; i < 128; ++i)
			if(histTemp0[0]->GetBinContent(i) && histTemp0[0]->GetBinContent(i) < minTemp[0])
				minTemp[0] = {histTemp0[0]->GetBinContent(i)};
	}

	if(normCount[1])
	{
		histTemp0[1]->Scale(100./normCount[1]);
		for(UChar_t i{1}; i < 128; ++i)
			if(histTemp0[1]->GetBinContent(i) && histTemp0[1]->GetBinContent(i) < minTemp[1])
				minTemp[1] = {histTemp0[1]->GetBinContent(i)};
	}

  if(bdNum != 6)
	{
		stemp[0] = {"Proton Timestamp-Spectrum: Board " + std::to_string(bdNum) + ", Channel "
						+ std::to_string(chNum)};
		stemp[1] = {"Proton 2D Energy-Timestamp-Spectrum: Board " + std::to_string(bdNum) + ", Channel "
						+ std::to_string(chNum)};
		style[0]->cd();
	  hist[2].push_back(new TH1D{"pTimeHist", stemp[0].c_str(), 500, 4.*pD.timeReq[0][0],
		4.*pD.timeReq[0][pD.numEnt - 1]});
	  hist[2].push_back(new TH2D{"pEnergyTimeHist", stemp[1].c_str(), 200, 4.*pD.timeReq[0][0],
		4.*pD.timeReq[0][pD.numEnt - 1], 40, 0., 40.});
		histTemp1 = {dynamic_cast<TH2D*>(hist[2][3])};

		for(Long64_t i{0}; i < pD.numEnt; ++i)
			if(bdNum == pD.bdNum[0][i] && chNum == pD.chNum[0][i])
			{
				hist[2][2]->Fill(4.*pD.timeReq[0][i]);
				histTemp1->Fill(4.*pD.timeReq[0][i], calib*pD.adcVal[0][i]);
				++normCount[2];
			}

		if(normCount[2])
		{
			for(Short_t i{}; i < 500; ++i)
				hist[2][2]->SetBinContent(i, 100.*hist[2][2]->GetBinContent(i)/normCount[2]);

			for(UChar_t i{}; i < 200; ++i)
				for(Char_t j{}; j < 40; ++j)
					histTemp1->SetBinContent(i, j, 100.*histTemp1->GetBinContent(i, j)/normCount[2]);
		}
	}
  else
	{
		style[0]->cd();
	  hist[2].push_back(new TH1D{"pTimeHist","Proton Timestamp-Spectrum: All Pixels", 500, 4.*pD.timeReq[0][0],
		4.*pD.timeReq[0][pD.numEnt - 1]});
	  hist[2].push_back(new TH2D{"pEnergyTimeHist","Proton 2D Energy-Timestamp-Spectrum:"
		"All Pixels", 200, 4.*pD.timeReq[0][0], 4.*pD.timeReq[0][pD.numEnt - 1], 40, 0., 40.});
		histTemp1 = {dynamic_cast<TH2D*>(hist[2][3])};

		for(Long64_t i{0}; i < pD.numEnt; ++i)
		{
			hist[2][2]->Fill(4.*pD.timeReq[0][i], 1./pD.numEnt);
			histTemp1->Fill(4.*pD.timeReq[0][i], calib*pD.adcVal[0][i], 1./pD.numEnt);
		}
	}

	//change to allTrig0 canvas, set y-axis to log-scale, create axis titles, and draw histogram
  gui->selCanvas(2, 0, 1);
	gPad->SetLogz();
	gPad->SetFrameLineColor(0);
	gPad->SetMargin(0., .15, .05, .2);
	hist[2][0]->SetZTitle("Yield (%)");
	hist[2][0]->SetMinimum(pow(10, floor(log10(minTemp[0]))));
	hist[2][0]->Draw("colz");
  gui->selCanvas(2, 0, 2);
	gPad->SetLogz();
	gPad->SetFrameLineColor(0);
	gPad->SetMargin(0., .15, .05, .2);
	hist[2][1]->SetZTitle("Yield (%)");
	hist[2][1]->SetMinimum(pow(10, floor(log10(minTemp[1]))));
	hist[2][1]->Draw("colz");
  gui->selCanvas(2, 0, 3);
	gPad->SetLogy();
  hist[2][2]->GetXaxis()->SetTitle("Proton Timestamp (ns)");
  hist[2][2]->GetYaxis()->SetTitle("Yield (%)");
  hist[2][2]->Draw();
  gui->selCanvas(2, 0, 4);
	gPad->SetLogz();
  hist[2][3]->GetXaxis()->SetTitle("Proton Timestamp (ns)");
  hist[2][3]->GetYaxis()->SetTitle("Proton Energy (keV)");
  hist[2][3]->GetZaxis()->SetTitle("Yield (%)");
  hist[2][3]->Draw("colz");
	gui->updateCanvas(2, 0);
}

//----------------------------------------------------------------------------------------

void Replay::plotProton1(Char_t bdNum, Char_t chNum)
{
	//instantiate and initialize variables
	std::string stemp[4]{};
	Long64_t normCount{};

	//delete existing histograms if applicable, create new histograms, and fill, depending upon pixel
	if(hist[3].size())
	{
	  delete hist[3][0];
		delete hist[3][1];
		delete hist[3][2];
		delete hist[3][3];
		hist[3].clear();
	}
  if(bdNum != 6)
	{
		stemp[0] = {"Proton Energy-Spectrum: Board " + std::to_string(bdNum) + ", Channel "
						+ std::to_string(chNum)};
		stemp[1] = {"Proton TOF-Spectrum: Board " + std::to_string(bdNum) + ", Channel "
						+ std::to_string(chNum)};
		stemp[2] = {"Proton Inverse-Squared-TOF-Spectrum: Board " + std::to_string(bdNum) + ", Channel "
						+ std::to_string(chNum)};
		stemp[3] = {"Proton Squared-Momentum-Spectrum w/ Geant4 Mapping: Board " + std::to_string(bdNum) + ", Channel "
						+ std::to_string(chNum)};
		style[0]->cd();
	  hist[3].push_back(new TH1D{"pEnergyHist", stemp[0].c_str(), 40, 0., 40.});
	  hist[3].push_back(new TH1D{"pTOFHist", stemp[1].c_str(), 200, 0., 80.});
	  hist[3].push_back(new TH1D{"pInvSquTOFHist", stemp[2].c_str(), 200, 0., .0065});
	  hist[3].push_back(new TH1D{"pSquMomHist", stemp[3].c_str(), 200, 0., 5.});

		for(Long64_t i{0}; i < pD.numEnt; ++i)
			if(bdNum == pD.bdNum[0][i] && chNum == pD.chNum[0][i])
			{
				hist[3][0]->Fill(calib*pD.adcVal[0][i]);
				hist[3][1]->Fill((pD.timeReq[0][i] - pD.timeReq[1][i])/250.);
				hist[3][2]->Fill(250.*250./(pD.timeReq[0][i] - pD.timeReq[1][i])/(pD.timeReq[0][i] - pD.timeReq[1][i]));
				hist[3][3]->Fill(1e-6*pD.protMom[i]*pD.protMom[i]);
				++normCount;
			}

		if(normCount)
		{
			for(Char_t i{}; i < 40; ++i)
				hist[3][0]->SetBinContent(i, 100.*hist[3][0]->GetBinContent(i)/normCount);

			for(UChar_t i{}; i < 200; ++i)
			{
				hist[3][1]->SetBinContent(i, 100.*hist[3][1]->GetBinContent(i)/normCount);
				hist[3][2]->SetBinContent(i, 100.*hist[3][2]->GetBinContent(i)/normCount);
				hist[3][3]->SetBinContent(i, 100.*hist[3][3]->GetBinContent(i)/normCount);
			}
		}
	}
  else
	{
		style[0]->cd();
	  hist[3].push_back(new TH1D{"pEnergyHist", "Proton Energy-Spectrum: All Pixels", 40, 0., 40.});
	  hist[3].push_back(new TH1D{"pTOFHist", "Proton TOF-Spectrum: All Pixels", 200, 0., 80.});
	  hist[3].push_back(new TH1D{"pInvSquTOFHist", "Proton Inverse-Squared-TOF-Spectrum: All Pixels", 200, 0., .0065});
	  hist[3].push_back(new TH1D{"pSquMomHist", "Proton Squared-Momentum-Spectrum w/ Geant4 Mapping: All Pixels", 200, 0., 1.6});

		for(Long64_t i{0}; i < pD.numEnt; ++i)
		{
			hist[3][0]->Fill(calib*pD.adcVal[0][i], 1./pD.numEnt);
			hist[3][1]->Fill((pD.timeReq[0][i] - pD.timeReq[1][i])/250., 1./pD.numEnt);
			hist[3][2]->Fill(250.*250./(pD.timeReq[0][i] - pD.timeReq[1][i])/(pD.timeReq[0][i] - pD.timeReq[1][i]), 1./pD.numEnt);
			hist[3][3]->Fill(1e-6*pD.protMom[i]*pD.protMom[i], 1./pD.numEnt);
		}
	}

	//change to allTrig0 canvas, set y-axis to log-scale, create axis titles, and draw histogram
  gui->selCanvas(3, 0, 1);
	gPad->SetLogy();
  hist[3][0]->GetXaxis()->SetTitle("Proton Energy (keV)");
  hist[3][0]->GetYaxis()->SetTitle("Yield (%)");
  hist[3][0]->Draw();
  gui->selCanvas(3, 0, 2);
	gPad->SetLogy();
  hist[3][1]->GetXaxis()->SetTitle("Proton TOF (us)");
  hist[3][1]->GetYaxis()->SetTitle("Yield (%)");
  hist[3][1]->Draw();
  gui->selCanvas(3, 0, 3);
	gPad->SetLogy();
  hist[3][2]->GetXaxis()->SetTitle("Proton TOF^-2 (us^-2)");
  hist[3][2]->GetYaxis()->SetTitle("Yield (%)");
  hist[3][2]->Draw();
  gui->selCanvas(3, 0, 4);
	gPad->SetLogy();
  hist[3][3]->GetXaxis()->SetTitle("Proton Squared-Momentum (MeV^2)");
  hist[3][3]->GetYaxis()->SetTitle("Yield (%)");
  hist[3][3]->Draw();
	gui->updateCanvas(3, 0);
}

//----------------------------------------------------------------------------------------

void Replay::plotElectron0(Char_t bdNum, Char_t chNum)
{
	//instantiate and initialize variables
	std::string stemp[2]{};
	const Double_t hexFact{sqrt(3.)/2.};
	const Double_t hexSide{.042};
	TH2Poly* histTemp0[2];
	TH2D* histTemp1;
	Double_t binCoord[2][6];
	Long64_t normCount[3]{};
	Double_t minTemp[]{100., 100.};

	//delete existing histograms if applicable, create new histograms, and fill, depending upon pixel
	if(hist[4].size())
	{
	  delete hist[4][0];
		delete hist[4][1];
		delete hist[4][2];
		delete hist[4][3];
		hist[4].clear();
	}
	style[4]->cd();
	hist[4].push_back(new TH2Poly{"pixMapElecUHist", "Electron Yield:  Upper Detector", 0., 1., 0., 1.});
	hist[4].push_back(new TH2Poly{"pixMapElecDHist", "Electron Yield:  Lower Detector", 0., 1., 0., 1.});
	histTemp0[0] = {dynamic_cast<TH2Poly*>(hist[4][0])};
	histTemp0[1] = {dynamic_cast<TH2Poly*>(hist[4][1])};
	for(Char_t i{0}, k{0}; i < 13; ++i)
		for(Char_t j{0}; j < (i < 7 ? i + 7 : 19 - i); ++j, ++k)
		{
			binCoord[0][0] = {1.5*hexSide*i + .101};
			binCoord[1][0] = {hexSide*hexFact*(i < 7 ? -i : i - 12) + 2.*hexSide*hexFact*j + .246};
			binCoord[0][1] = {binCoord[0][0] + hexSide};
			binCoord[1][1] = {binCoord[1][0]};
			binCoord[0][2] = {binCoord[0][1] + hexSide/2.};
			binCoord[1][2] = {binCoord[1][1] + hexSide*hexFact};
			binCoord[0][3] = {binCoord[0][1]};
			binCoord[1][3] = {binCoord[1][2] + hexSide*hexFact};
			binCoord[0][4] = {binCoord[0][0]};
			binCoord[1][4] = {binCoord[1][3]};
			binCoord[0][5] = {binCoord[0][4] - hexSide/2.};
			binCoord[1][5] = {binCoord[1][2]};
			histTemp0[0]->AddBin(6, binCoord[0], binCoord[1]);
			histTemp0[1]->AddBin(6, binCoord[0], binCoord[1]);
			binCenter[0][k + (i < 7 ? i + 7 : 19 - i) - 2*j - 1] = {binCoord[0][5] + hexSide};
			binCenter[1][k + (i < 7 ? i + 7 : 19 - i) - 2*j - 1] = {binCoord[1][5]};
		}

	for(Long64_t i{0}; i < pD.numEnt; ++i)
		if(bdChPixel[8*pD.bdNum[1][i] + pD.chNum[1][i]] != 0)
		{
			if(bdChPixel[8*pD.bdNum[1][i] + pD.chNum[1][i]] < 128)
			{
				histTemp0[0]->Fill(binCenter[0][bdChPixel[8*pD.bdNum[1][i] + pD.chNum[1][i]] - 1],
				binCenter[1][bdChPixel[8*pD.bdNum[1][i] + pD.chNum[1][i]] - 1]);
				++normCount[0];
			}
			else
			{
				histTemp0[1]->Fill(binCenter[0][bdChPixel[8*pD.bdNum[1][i] + pD.chNum[1][i]] - 129],
				binCenter[1][bdChPixel[8*pD.bdNum[1][i] + pD.chNum[1][i]] - 129]);
				++normCount[1];
			}
		}

	if(normCount[0])
	{
		histTemp0[0]->Scale(100./normCount[0]);
		for(UChar_t i{1}; i < 128; ++i)
			if(histTemp0[0]->GetBinContent(i) && histTemp0[0]->GetBinContent(i) < minTemp[0])
				minTemp[0] = {histTemp0[0]->GetBinContent(i)};
	}

	if(normCount[1])
	{
		histTemp0[1]->Scale(100./normCount[1]);
		for(UChar_t i{1}; i < 128; ++i)
			if(histTemp0[1]->GetBinContent(i) && histTemp0[1]->GetBinContent(i) < minTemp[1])
				minTemp[1] = {histTemp0[1]->GetBinContent(i)};
	}

  if(bdNum != 6)
	{
		stemp[0] = {"Electron Timestamp-Spectrum: Board " + std::to_string(bdNum) + ", Channel "
						+ std::to_string(chNum)};
		stemp[1] = {"Electron 2D Energy-Timestamp-Spectrum: Board " + std::to_string(bdNum) + ", Channel "
						+ std::to_string(chNum)};
		style[0]->cd();
	  hist[4].push_back(new TH1D{"eTimeHist", stemp[0].c_str(), 500, 4.*pD.timeReq[1][0],
		4.*pD.timeReq[1][pD.numEnt - 1]});
	  hist[4].push_back(new TH2D{"eEnergyTimeHist", stemp[1].c_str(), 200, 4.*pD.timeReq[1][0],
		4.*pD.timeReq[1][pD.numEnt - 1], 300, 0., 900.});
		histTemp1 = {dynamic_cast<TH2D*>(hist[4][3])};

		for(Long64_t i{0}; i < pD.numEnt; ++i)
			if(bdNum == pD.bdNum[1][i] && chNum == pD.chNum[1][i])
			{
				hist[4][2]->Fill(4.*pD.timeReq[1][i]);
				histTemp1->Fill(4.*pD.timeReq[1][i], calib*pD.adcVal[1][i]);
				++normCount[2];
			}

		if(normCount[2])
		{
			for(Short_t i{}; i < 500; ++i)
				hist[4][2]->SetBinContent(i, 100.*hist[4][2]->GetBinContent(i)/normCount[2]);

			for(UChar_t i{}; i < 200; ++i)
				for(Short_t j{}; j < 300; ++j)
					histTemp1->SetBinContent(i, j, 100.*histTemp1->GetBinContent(i, j)/normCount[2]);
		}
	}
  else
	{
		style[0]->cd();
	  hist[4].push_back(new TH1D{"eTimeHist","Electron Timestamp-Spectrum: All Pixels", 500, 4.*pD.timeReq[1][0],
		4.*pD.timeReq[1][pD.numEnt - 1]});
	  hist[4].push_back(new TH2D{"eEnergyTimeHist","Electron 2D Energy-Timestamp-Spectrum:"
		"All Pixels", 200, 4.*pD.timeReq[1][0], 4.*pD.timeReq[1][pD.numEnt - 1], 300, 0., 900.});
		histTemp1 = {dynamic_cast<TH2D*>(hist[4][3])};

		for(Long64_t i{0}; i < pD.numEnt; ++i)
		{
			hist[4][2]->Fill(4.*pD.timeReq[1][i], 1./pD.numEnt);
			histTemp1->Fill(4.*pD.timeReq[1][i], calib*pD.adcVal[1][i], 1./pD.numEnt);
		}
	}

	//change to allTrig0 canvas, set y-axis to log-scale, create axis titles, and draw histogram
  gui->selCanvas(4, 0, 1);
	gPad->SetLogz();
	gPad->SetFrameLineColor(0);
	gPad->SetMargin(0., .15, .05, .2);
	hist[4][0]->SetZTitle("Yield (%)");
	hist[4][0]->SetMinimum(pow(10, floor(log10(minTemp[0]))));
	hist[4][0]->Draw("colz");
  gui->selCanvas(4, 0, 2);
	gPad->SetLogz();
	gPad->SetFrameLineColor(0);
	gPad->SetMargin(0., .15, .05, .2);
	hist[4][1]->SetZTitle("Yield (%)");
	hist[4][1]->SetMinimum(pow(10, floor(log10(minTemp[1]))));
	hist[4][1]->Draw("colz");
  gui->selCanvas(4, 0, 3);
	gPad->SetLogy();
  hist[4][2]->GetXaxis()->SetTitle("Electron Timestamp (ns)");
  hist[4][2]->GetYaxis()->SetTitle("Yield (%)");
  hist[4][2]->Draw();
  gui->selCanvas(4, 0, 4);
	gPad->SetLogz();
  hist[4][3]->GetXaxis()->SetTitle("Electron Timestamp (ns)");
  hist[4][3]->GetYaxis()->SetTitle("Electron Energy (keV)");
  hist[4][3]->GetZaxis()->SetTitle("Yield (%)");
  hist[4][3]->Draw("colz");
	gui->updateCanvas(4, 0);
}

//----------------------------------------------------------------------------------------

void Replay::plotElectron1(Char_t bdNum, Char_t chNum)
{
	//instantiate and initialize variables
	std::string stemp[2]{};
	Long64_t normCount[2]{};

	//delete existing histograms if applicable, create new histograms, and fill, depending upon pixel
	if(hist[5].size())
	{
	  delete hist[5][0];
		delete hist[5][1];
		hist[5].clear();
	}
  if(bdNum != 6)
	{
		stemp[0] = {"Electron Energy-Spectrum (w/o Signal Reconstruction): Board " + std::to_string(bdNum) + ", Channel "
						+ std::to_string(chNum)};
		stemp[1] = {"Electron Energy-Spectrum: Board " + std::to_string(bdNum) + ", Channel "
						+ std::to_string(chNum)};
		style[1]->cd();
	  hist[5].push_back(new TH1D{"eEnergyWOSigRecHist", stemp[0].c_str(), 300, 0., 900.});
	  hist[5].push_back(new TH1D{"eEnergyHist", stemp[1].c_str(), 300, 0., 900.});

		for(Long64_t i{0}; i < tD.numEnt; ++i)
			if(bdNum == tD.bdNum[i] && chNum == tD.chNum[i] && partIndex[0][i] > 1)
			{
					hist[5][0]->Fill(calib*tD.adcVal[i]);
					++normCount[0];
			}

		for(Long64_t i{0}; i < pD.numEnt; ++i)
			if(bdNum == pD.bdNum[1][i] && chNum == pD.chNum[1][i])
			{
				hist[5][1]->Fill(calib*pD.adcVal[1][i]);
				++normCount[1];
			}

		if(normCount[0])
			for(Short_t i{}; i < 300; ++i)
				hist[5][0]->SetBinContent(i, 100.*hist[5][0]->GetBinContent(i)/normCount[0]);

		if(normCount[1])
			for(Short_t i{}; i < 300; ++i)
				hist[5][1]->SetBinContent(i, 100.*hist[5][1]->GetBinContent(i)/normCount[1]);

	}
  else
	{
		style[1]->cd();
	  hist[5].push_back(new TH1D{"eEnergyWOSigRecHist", "Electron Energy-Spectrum (w/o Signal Reconstruction): All Pixels", 300, 0., 900.});
	  hist[5].push_back(new TH1D{"eEnergyHist", "Electron Energy-Spectrum: All Pixels", 300, 0., 900.});

		for(Long64_t i{0}; i < tD.numEnt; ++i)
			if(partIndex[0][i] > 1)
					hist[5][0]->Fill(calib*tD.adcVal[i], 1./tD.numEnt);

		for(Long64_t i{0}; i < pD.numEnt; ++i)
			hist[5][1]->Fill(calib*pD.adcVal[1][i], 1./pD.numEnt);
	}

	//change to allTrig0 canvas, set y-axis to log-scale, create axis titles, and draw histogram
  gui->selCanvas(5, 0, 1);
	gPad->SetLogy();
  hist[5][0]->GetXaxis()->SetTitle("Electron Energy (keV)");
  hist[5][0]->GetYaxis()->SetTitle("Yield (%)");
  hist[5][0]->Draw();
  gui->selCanvas(5, 0, 2);
	gPad->SetLogy();
  hist[5][1]->GetXaxis()->SetTitle("Electron Energy (keV)");
  hist[5][1]->GetYaxis()->SetTitle("Yield (%)");
  hist[5][1]->Draw();
	gui->updateCanvas(5, 0);
}

//----------------------------------------------------------------------------------------

void Replay::plotOther0(Char_t bdNum, Char_t chNum)
{
	//instantiate and initialize variables
	std::string stemp[2]{};
	const Double_t hexFact{sqrt(3.)/2.};
	const Double_t hexSide{.042};
	TH2Poly* histTemp0[2];
	TH2D* histTemp1;
	Double_t binCoord[2][6];
	Long64_t normCount[3]{};
	Double_t minTemp[]{100., 100.};

	//delete existing histograms if applicable, create new histograms, and fill, depending upon pixel
	if(hist[6].size())
	{
	  delete hist[6][0];
		delete hist[6][1];
		delete hist[6][2];
		delete hist[6][3];
		hist[6].clear();
	}
	style[4]->cd();
	hist[6].push_back(new TH2Poly{"pixMapOtherUHist", "Unidentified Particle Yield:  Upper Detector", 0., 1., 0., 1.});
	hist[6].push_back(new TH2Poly{"pixMapOtherDHist", "Unidentified Particle Yield:  Lower Detector", 0., 1., 0., 1.});
	histTemp0[0] = {dynamic_cast<TH2Poly*>(hist[6][0])};
	histTemp0[1] = {dynamic_cast<TH2Poly*>(hist[6][1])};
	for(Char_t i{0}, k{0}; i < 13; ++i)
		for(Char_t j{0}; j < (i < 7 ? i + 7 : 19 - i); ++j, ++k)
		{
			binCoord[0][0] = {1.5*hexSide*i + .101};
			binCoord[1][0] = {hexSide*hexFact*(i < 7 ? -i : i - 12) + 2.*hexSide*hexFact*j + .246};
			binCoord[0][1] = {binCoord[0][0] + hexSide};
			binCoord[1][1] = {binCoord[1][0]};
			binCoord[0][2] = {binCoord[0][1] + hexSide/2.};
			binCoord[1][2] = {binCoord[1][1] + hexSide*hexFact};
			binCoord[0][3] = {binCoord[0][1]};
			binCoord[1][3] = {binCoord[1][2] + hexSide*hexFact};
			binCoord[0][4] = {binCoord[0][0]};
			binCoord[1][4] = {binCoord[1][3]};
			binCoord[0][5] = {binCoord[0][4] - hexSide/2.};
			binCoord[1][5] = {binCoord[1][2]};
			histTemp0[0]->AddBin(6, binCoord[0], binCoord[1]);
			histTemp0[1]->AddBin(6, binCoord[0], binCoord[1]);
			binCenter[0][k + (i < 7 ? i + 7 : 19 - i) - 2*j - 1] = {binCoord[0][5] + hexSide};
			binCenter[1][k + (i < 7 ? i + 7 : 19 - i) - 2*j - 1] = {binCoord[1][5]};
		}

	for(Long64_t i{0}; i < tD.numEnt; ++i)
		if(!partIndex[0][i] && bdChPixel[8*tD.bdNum[i] + tD.chNum[i]])
		{
			if(bdChPixel[8*tD.bdNum[i] + tD.chNum[i]] < 128)
			{
				histTemp0[0]->Fill(binCenter[0][bdChPixel[8*tD.bdNum[i] + tD.chNum[i]] - 1],
				binCenter[1][bdChPixel[8*tD.bdNum[i] + tD.chNum[i]] - 1]);
				++normCount[0];
			}
			else
			{
				histTemp0[1]->Fill(binCenter[0][bdChPixel[8*tD.bdNum[i] + tD.chNum[i]] - 129],
				binCenter[1][bdChPixel[8*tD.bdNum[i] + tD.chNum[i]] - 129]);
				++normCount[1];
			}
		}

	if(normCount[0])
	{
		histTemp0[0]->Scale(100./normCount[0]);
		for(UChar_t i{1}; i < 128; ++i)
			if(histTemp0[0]->GetBinContent(i) && histTemp0[0]->GetBinContent(i) < minTemp[0])
				minTemp[0] = {histTemp0[0]->GetBinContent(i)};
	}

	if(normCount[1])
	{
		histTemp0[1]->Scale(100./normCount[1]);
		for(UChar_t i{1}; i < 128; ++i)
			if(histTemp0[1]->GetBinContent(i) && histTemp0[1]->GetBinContent(i) < minTemp[1])
				minTemp[1] = {histTemp0[1]->GetBinContent(i)};
	}

  if(bdNum != 6)
	{
		stemp[0] = {"Unidentified Particle Timestamp-Spectrum: Board " + std::to_string(bdNum) + ", Channel "
						+ std::to_string(chNum)};
		stemp[1] = {"Unidentified Particle 2D Energy-Timestamp-Spectrum: Board " + std::to_string(bdNum) + ", Channel "
						+ std::to_string(chNum)};
		style[0]->cd();
	  hist[6].push_back(new TH1D{"oTimeHist", stemp[0].c_str(), 500, 4.*tD.timeReq[0],
		4.*tD.timeReq[tD.numEnt - 1]});
	  hist[6].push_back(new TH2D{"oEnergyTimeHist", stemp[1].c_str(), 200, 4.*tD.timeReq[0],
		4.*tD.timeReq[tD.numEnt - 1], 300, 0., 900.});
		histTemp1 = {dynamic_cast<TH2D*>(hist[6][3])};

		for(Long64_t i{0}; i < tD.numEnt; ++i)
			if(!partIndex[0][i] && bdNum == tD.bdNum[i] && chNum == tD.chNum[i])
			{
				hist[6][2]->Fill(4.*tD.timeReq[i]);
				histTemp1->Fill(4.*tD.timeReq[i], calib*tD.adcVal[i]);
				++normCount[2];
			}

		if(normCount[2])
		{
			for(Short_t i{}; i < 500; ++i)
				hist[6][2]->SetBinContent(i, 100.*hist[6][2]->GetBinContent(i)/normCount[2]);

			for(UChar_t i{}; i < 200; ++i)
				for(Short_t j{}; j < 300; ++j)
					histTemp1->SetBinContent(i, j, 100.*histTemp1->GetBinContent(i, j)/normCount[2]);
		}
	}
  else
	{
		style[0]->cd();
	  hist[6].push_back(new TH1D{"oTimeHist","Unidentified Particle Timestamp-Spectrum: All Pixels", 500, 4.*tD.timeReq[0],
		4.*tD.timeReq[tD.numEnt - 1]});
	  hist[6].push_back(new TH2D{"oEnergyTimeHist","Unidentified Particle 2D Energy-Timestamp-Spectrum:"
		"All Pixels", 200, 4.*tD.timeReq[0], 4.*tD.timeReq[tD.numEnt - 1], 300, 0., 900.});
		histTemp1 = {dynamic_cast<TH2D*>(hist[6][3])};

		for(Long64_t i{0}; i < tD.numEnt; ++i)
			if(!partIndex[0][i])
			{
				hist[6][2]->Fill(4.*tD.timeReq[i], 1./tD.numEnt);
				histTemp1->Fill(4.*tD.timeReq[i], calib*tD.adcVal[i], 1./tD.numEnt);
			}
	}

	//change to allTrig0 canvas, set y-axis to log-scale, create axis titles, and draw histogram
  gui->selCanvas(6, 0, 1);
	gPad->SetLogz();
	gPad->SetFrameLineColor(0);
	gPad->SetMargin(0., .15, .05, .2);
	hist[6][0]->SetZTitle("Yield (%)");
	hist[6][0]->SetMinimum(pow(10, floor(log10(minTemp[0]))));
	hist[6][0]->Draw("colz");
  gui->selCanvas(6, 0, 2);
	gPad->SetLogz();
	gPad->SetFrameLineColor(0);
	gPad->SetMargin(0., .15, .05, .2);
	hist[6][1]->SetZTitle("Yield (%)");
	hist[6][1]->SetMinimum(pow(10, floor(log10(minTemp[1]))));
	hist[6][1]->Draw("colz");
  gui->selCanvas(6, 0, 3);
	gPad->SetLogy();
  hist[6][2]->GetXaxis()->SetTitle("Unidentified Particle Timestamp (ns)");
  hist[6][2]->GetYaxis()->SetTitle("Yield (%)");
  hist[6][2]->Draw();
  gui->selCanvas(6, 0, 4);
	gPad->SetLogz();
  hist[6][3]->GetXaxis()->SetTitle("Unidentified Particle Timestamp (ns)");
  hist[6][3]->GetYaxis()->SetTitle("Unidentified Particle Energy (keV)");
  hist[6][3]->GetZaxis()->SetTitle("Yield (%)");
  hist[6][3]->Draw("colz");
	gui->updateCanvas(6, 0);
}

//----------------------------------------------------------------------------------------

void Replay::plotOther1(Char_t bdNum, Char_t chNum)
{
	//instantiate and initialize variables
	std::string stemp{};
	Long64_t normCount{};

	//delete existing histograms if applicable, create new histograms, and fill, depending upon pixel
	if(hist[7].size())
	{
	  delete hist[7][0];
		hist[7].clear();
	}
  if(bdNum != 6)
	{
		stemp = {"Unidentified Particle Energy-Spectrum: Board " + std::to_string(bdNum) + ", Channel "
						+ std::to_string(chNum)};
		style[2]->cd();
	  hist[7].push_back(new TH1D{"oEnergyHist", stemp.c_str(), 300, 0., 900.});

		for(Long64_t i{0}; i < tD.numEnt; ++i)
			if(!partIndex[0][i] && bdNum == tD.bdNum[i] && chNum == tD.chNum[i])
			{
					hist[7][0]->Fill(calib*tD.adcVal[i]);
					++normCount;
			}

		if(normCount)
			for(Short_t i{}; i < 300; ++i)
				hist[7][0]->SetBinContent(i, 100.*hist[7][0]->GetBinContent(i)/normCount);
	}
  else
	{
		style[2]->cd();
	  hist[7].push_back(new TH1D{"oEnergyHist", "Unidentified Particle Energy-Spectrum: All Pixels", 300, 0., 900.});

		for(Long64_t i{0}; i < tD.numEnt; ++i)
			if(!partIndex[0][i])
					hist[7][0]->Fill(calib*tD.adcVal[i], 1./tD.numEnt);
	}

	//change to allTrig0 canvas, set y-axis to log-scale, create axis titles, and draw histogram
  gui->selCanvas(7, 0, 0);
	gPad->SetLogy();
  hist[7][0]->GetXaxis()->SetTitle("Unidentified Particle Energy (keV)");
  hist[7][0]->GetYaxis()->SetTitle("Yield (%)");
  hist[7][0]->Draw();
	gui->updateCanvas(7, 0);
}

//----------------------------------------------------------------------------------------

void Replay::plotPair0()
{
	//instantiate and initialize variables
	TH2D* histTemp[2];

	//delete existing histograms if applicable, create new histograms, and fill, depending upon pixel
	if(hist[8].size())
	{
	  delete hist[8][0];
		delete hist[8][1];
		hist[8].clear();
	}

	style[1]->cd();
  hist[8].push_back(new TH2D{"eEnergyPInvSquTOFHist", "Electron Energy vs. Proton Inverse-Squared-TOF: All Pixels", 200, 0., .0065, 300, 0., 900.});
	histTemp[0] = {dynamic_cast<TH2D*>(hist[8][0])};
  hist[8].push_back(new TH2D{"eEnergyPSquMomHist", "Electron Energy vs. Proton Squared-Momentum w/ Geant4 Mapping: All Pixels", 200, 0., 1.6, 300, 0., 900.});
	histTemp[1] = {dynamic_cast<TH2D*>(hist[8][1])};

	for(Long64_t i{0}; i < pD.numEnt; ++i)
	{
		histTemp[0]->Fill(250.*250./(pD.timeReq[0][i] - pD.timeReq[1][i])/(pD.timeReq[0][i] - pD.timeReq[1][i]), calib*pD.adcVal[1][i], 1./pD.numEnt);
		histTemp[1]->Fill(1e-6*pD.protMom[i]*pD.protMom[i], calib*pD.adcVal[1][i], 1./pD.numEnt);
	}

	//change to allTrig0 canvas, set y-axis to log-scale, create axis titles, and draw histogram
  gui->selCanvas(8, 0, 1);
  hist[8][0]->GetXaxis()->SetTitle("Proton TOF^-2 (us^-2)");
  hist[8][0]->GetYaxis()->SetTitle("Electron Energy (keV)");
  hist[8][0]->GetZaxis()->SetTitle("Yield (%)");
  hist[8][0]->Draw("colz");
  gui->selCanvas(8, 0, 2);
  hist[8][1]->GetXaxis()->SetTitle("Proton Squared-Momentum (MeV^2)");
  hist[8][1]->GetYaxis()->SetTitle("Electron Energy (keV)");
  hist[8][1]->GetZaxis()->SetTitle("Yield (%)");
  hist[8][1]->Draw("colz");
	gui->updateCanvas(8, 0);
}
//----------------------------------------------------------------------------------------

void Replay::plotPair1()
{
	//instantiate and initialize variables
	TH2D* histTemp;

	//delete existing histograms if applicable, create new histograms, and fill, depending upon pixel
	if(hist[9].size())
	{
	  delete hist[9][0];
		hist[9].clear();
	}

	style[2]->cd();
  hist[9].push_back(new TH2D{"eEnergyCosThetaHist", "Electron Energy vs. CosTheta w/ Geant4 Mapping: All Pixels", 200, -1.2, 1.2, 300, 0., 900.});
	histTemp = {dynamic_cast<TH2D*>(hist[9][0])};

	for(Long64_t i{0}; i < pD.numEnt; ++i)
		histTemp->Fill(pD.cosTheta[i], calib*pD.adcVal[1][i], 1./pD.numEnt);

	//change to allTrig0 canvas, set y-axis to log-scale, create axis titles, and draw histogram
  gui->selCanvas(9, 0, 0);
  hist[9][0]->GetXaxis()->SetTitle("cosTheta");
  hist[9][0]->GetYaxis()->SetTitle("Electron Energy (keV)");
  hist[9][0]->GetZaxis()->SetTitle("Yield (%)");
  hist[9][0]->Draw("colz");
	gui->updateCanvas(9, 0);
}

//----------------------------------------------------------------------------------------

void Replay::plotPair2()
{
	//create variables
	Double_t zeroPtTemp{};

	//delete existing histograms if applicable, create new histograms, and fill, depending upon pixel
	if(hist[10].size())
	{
		for(Char_t i{}; i < 6; ++i)
		{
			delete hist[10][i];
			delete funct[10][i];
		}

		hist[10].clear();
		funct[10].clear();
	}

	style[3]->cd();
  hist[10].push_back(new TH1D{"cosThetaE110Hist", "CosTheta Spectrum w/ Geant4 Mapping (Electron Energy 100 - 120 keV): All Pixels", 200, -1.2, 1.2});
  hist[10].push_back(new TH1D{"cosThetaE210Hist", "CosTheta Spectrum w/ Geant4 Mapping (Electron Energy 200 - 220 keV): All Pixels", 200, -1.2, 1.2});
  hist[10].push_back(new TH1D{"cosThetaE310Hist", "CosTheta Spectrum w/ Geant4 Mapping (Electron Energy 300 - 320 keV): All Pixels", 200, -1.2, 1.2});
  hist[10].push_back(new TH1D{"cosThetaE410Hist", "CosTheta Spectrum w/ Geant4 Mapping (Electron Energy 400 - 420 keV): All Pixels", 200, -1.2, 1.2});
  hist[10].push_back(new TH1D{"cosThetaE510Hist", "CosTheta Spectrum w/ Geant4 Mapping (Electron Energy 500 - 520 keV): All Pixels", 200, -1.2, 1.2});
  hist[10].push_back(new TH1D{"cosThetaE610Hist", "CosTheta Spectrum w/ Geant4 Mapping (Electron Energy 600 - 620 keV): All Pixels", 200, -1.2, 1.2});

	for(Long64_t i{}; i < pD.numEnt; ++i)
	{
		if(calib*pD.adcVal[1][i] >= 100. && calib*pD.adcVal[1][i] < 120.)
			hist[10][0]->Fill(pD.cosTheta[i]);
		if(calib*pD.adcVal[1][i] >= 200. && calib*pD.adcVal[1][i] < 220.)
			hist[10][1]->Fill(pD.cosTheta[i]);
		if(calib*pD.adcVal[1][i] >= 300. && calib*pD.adcVal[1][i] < 320.)
			hist[10][2]->Fill(pD.cosTheta[i]);
		if(calib*pD.adcVal[1][i] >= 400. && calib*pD.adcVal[1][i] < 420.)
			hist[10][3]->Fill(pD.cosTheta[i]);
		if(calib*pD.adcVal[1][i] >= 500. && calib*pD.adcVal[1][i] < 520.)
			hist[10][4]->Fill(pD.cosTheta[i]);
		if(calib*pD.adcVal[1][i] >= 600. && calib*pD.adcVal[1][i] < 620.)
			hist[10][5]->Fill(pD.cosTheta[i]);
	}
	
	//create functions
	funct[10].push_back(new TF1{"cosThetaE110Funct", "[0]*x+[1]", -1., 1.});
	funct[10].push_back(new TF1{"cosThetaE210Funct", "[0]*x+[1]", -1., 1.});
	funct[10].push_back(new TF1{"cosThetaE310Funct", "[0]*x+[1]", -1., 1.});
	funct[10].push_back(new TF1{"cosThetaE410Funct", "[0]*x+[1]", -1., 1.});
	funct[10].push_back(new TF1{"cosThetaE510Funct", "[0]*x+[1]", -1., 1.});
	funct[10].push_back(new TF1{"cosThetaE610Funct", "[0]*x+[1]", -1., 1.});

	for(Char_t i{}; i < 10; ++i)
		zeroPtTemp += hist[10][0]->GetBinContent(96 + i);
	zeroPtTemp /= 10.;
	funct[10][0]->SetParameters(zeroPtTemp*geantSimA*sqrt(1. - massElec*massElec/(massElec + 110.)/(massElec + 110.)), zeroPtTemp);

	
	zeroPtTemp = {};
	for(Char_t i{}; i < 10; ++i)
		zeroPtTemp += hist[10][1]->GetBinContent(96 + i);
	zeroPtTemp /= 10.;
	funct[10][1]->SetParameters(zeroPtTemp*geantSimA*sqrt(1. - massElec*massElec/(massElec + 110.)/(massElec + 110.)), zeroPtTemp);

	zeroPtTemp = {};
	for(Char_t i{}; i < 10; ++i)
		zeroPtTemp += hist[10][2]->GetBinContent(96 + i);
	zeroPtTemp /= 10.;
	funct[10][2]->SetParameters(zeroPtTemp*geantSimA*sqrt(1. - massElec*massElec/(massElec + 110.)/(massElec + 110.)), zeroPtTemp);

	zeroPtTemp = {};
	for(Char_t i{}; i < 10; ++i)
		zeroPtTemp += hist[10][3]->GetBinContent(96 + i);
	zeroPtTemp /= 10.;
	funct[10][3]->SetParameters(zeroPtTemp*geantSimA*sqrt(1. - massElec*massElec/(massElec + 110.)/(massElec + 110.)), zeroPtTemp);

	zeroPtTemp = {};
	for(Char_t i{}; i < 10; ++i)
		zeroPtTemp += hist[10][4]->GetBinContent(96 + i);
	zeroPtTemp /= 10.;
	funct[10][4]->SetParameters(zeroPtTemp*geantSimA*sqrt(1. - massElec*massElec/(massElec + 110.)/(massElec + 110.)), zeroPtTemp);

	zeroPtTemp = {};
	for(Char_t i{}; i < 10; ++i)
		zeroPtTemp += hist[10][5]->GetBinContent(96 + i);
	zeroPtTemp /= 10.;
	funct[10][5]->SetParameters(zeroPtTemp*geantSimA*sqrt(1. - massElec*massElec/(massElec + 110.)/(massElec + 110.)), zeroPtTemp);
	
	
	//change to allTrig0 canvas, set y-axis to log-scale, create axis titles, and draw histogram
  gui->selCanvas(10, 0, 1);
  hist[10][0]->GetXaxis()->SetTitle("cosTheta");
  hist[10][0]->GetYaxis()->SetTitle("Yield");
  hist[10][0]->Draw();
  	if(!pD.flag) linFitCorPar[5]->Draw("same");
	funct[10][0]->SetLineColor(kBlack);
	funct[10][0]->Draw("same");
	
 
  gui->selCanvas(10, 0, 2);
  hist[10][1]->GetXaxis()->SetTitle("cosTheta");
  hist[10][1]->GetYaxis()->SetTitle("Yield");
  hist[10][1]->Draw();
	if(!pD.flag) linFitCorPar[10]->Draw("same");
	funct[10][1]->SetLineColor(kBlack);
	funct[10][1]->Draw("same");
  gui->selCanvas(10, 0, 3);
  hist[10][2]->GetXaxis()->SetTitle("cosTheta");
  hist[10][2]->GetYaxis()->SetTitle("Yield");
  hist[10][2]->Draw();
	if(!pD.flag) linFitCorPar[15]->Draw("same");
	funct[10][2]->SetLineColor(kBlack);
	funct[10][2]->Draw("same");
  gui->selCanvas(10, 0, 4);
  hist[10][3]->GetXaxis()->SetTitle("cosTheta");
  hist[10][3]->GetYaxis()->SetTitle("Yield");
  hist[10][3]->Draw();
	if(!pD.flag) linFitCorPar[20]->Draw("same");
	funct[10][3]->SetLineColor(kBlack);
	funct[10][3]->Draw("same");
  gui->selCanvas(10, 0, 5);
  hist[10][4]->GetXaxis()->SetTitle("cosTheta");
  hist[10][4]->GetYaxis()->SetTitle("Yield");
  hist[10][4]->Draw();
	if(!pD.flag) linFitCorPar[25]->Draw("same");
	funct[10][4]->SetLineColor(kBlack);
	funct[10][4]->Draw("same");
  gui->selCanvas(10, 0, 6);
  hist[10][5]->GetXaxis()->SetTitle("cosTheta");
  hist[10][5]->GetYaxis()->SetTitle("Yield");
  hist[10][5]->Draw();
	if(!pD.flag) linFitCorPar[30]->Draw("same");
	funct[10][5]->SetLineColor(kBlack);
	funct[10][5]->Draw("same");
	gui->updateCanvas(10, 0);
}

//----------------------------------------------------------------------------------------

void Replay::plotPair3()
{
	//instantiate and initialize variables
	TH2D* histTemp;

	//delete existing histograms if applicable, create new histograms, and fill, depending upon pixel
	if(hist[11].size())
	{
	  delete hist[11][0];
		delete hist[11][1];
		delete hist[11][2];
		hist[11].clear();
	}

	style[1]->cd();
  hist[11].push_back(new TH2D{"pSigCntESigCntHist", "# of Proton Signals vs # of Electron Signals per Reconstructed Pair: All Pixels", 10, .5, 10.5, 3, .5, 3.5});
	histTemp = {dynamic_cast<TH2D*>(hist[11][0])};
  hist[11].push_back(new TH1D{"spectMapPlot0", "Spectrometer Mapping via Geant4 Simulation", 150, 0., .0065});
  hist[11].push_back(new TH1D{"spectMapPlot1", "", 150, 0., .0065});

	for(Long64_t i{}; i < pD.numEnt; ++i)
		histTemp->Fill(pD.partNum[1][i], pD.partNum[0][i], 1./pD.numEnt);

  for(UChar_t i{}; i < 150; ++i)
	{
		hist[11][1]->SetBinContent(i+1, spectMapProj[0][i][0]);
		hist[11][2]->SetBinContent(i+1, spectMapProj[0][i][1]);
	}

	//change to allTrig0 canvas, set y-axis to log-scale, create axis titles, and draw histogram
  gui->selCanvas(11, 0, 1);
	gPad->SetLogz();
  hist[11][0]->GetXaxis()->SetTitle("# of Protons");
  hist[11][0]->GetYaxis()->SetTitle("# of Electrons");
  hist[11][0]->GetZaxis()->SetTitle("Yield (%)");
  hist[11][0]->Draw("colz");
  gui->selCanvas(11, 0, 2);
	gPad->SetLogz();
  hist[11][1]->GetXaxis()->SetTitle("Proton TOF (us)");
  hist[11][1]->GetYaxis()->SetTitle("Spectrometer Length (m)");
  hist[11][1]->Draw();
	hist[11][2]->SetLineColor(kRed);
	hist[11][2]->Draw("same");
	gui->updateCanvas(11, 0);
}

//----------------------------------------------------------------------------------------

void Replay::plotAExtraction()
{
	//delete existing histograms if applicable, create new histograms, and fill, depending upon pixel
	if(hist[12].size())
	{
	  delete hist[12][0];
		delete hist[12][1];
		hist[12].clear();
	}

	style[1]->cd();
  hist[12].push_back(new TH1D{"aExtractPlot", "Value of #it{a} from Current Data Batch", 39, 0., 780.});
  hist[12].push_back(new TH1D{"aExtractTotalPlot", "Value of #it{a} from All Data", 39, 0., 780.});

	for(Char_t i{}; i < 39; ++i)
		if(valueCorPar[i][0] != 1000.)
		{
			hist[12][0]->SetBinContent(i+1, valueCorPar[i][0]);
			hist[12][0]->SetBinError(i+1, valueCorPar[i][1]);
			hist[12][1]->SetBinContent(i+1, valCorParTot[i][0]);
			hist[12][1]->SetBinError(i+1, valCorParTot[i][1]);
		}

	//change to allTrig0 canvas, set y-axis to log-scale, create axis titles, and draw histogram
  gui->selCanvas(12, 0, 1);
  hist[12][0]->GetXaxis()->SetTitle("Electron Energy (keV)");
  hist[12][0]->GetYaxis()->SetTitle("Value of #it{a}");
  hist[12][0]->Draw();
  gui->selCanvas(12, 0, 2);
  hist[12][1]->GetXaxis()->SetTitle("Electron Energy (keV)");
  hist[12][1]->GetYaxis()->SetTitle("Value of #it{a}");
  hist[12][1]->Draw();
	gui->updateCanvas(12, 0);
}

//----------------------------------------------------------------------------------------

//LDM3
void Replay::plotWaveforms()
{
	//delete existing histograms if applicable, create new histograms, and fill, depending upon pixel
	if(hist2D[0].size())
	{
	  	delete hist2D[0][0];
		delete hist2D[0][1];
		hist2D[0].clear();
	}
	
	style[1]->cd();
	hist2D[0].push_back(new TH2D{"wfs",Form("First 500 Waveforms from Wf#%i",Int_t(wfD.wf_Num) ), Int_t(wfD.maxWfLen),0,Double_t(wfD.maxWfLen),500,0,500});
	
	for (Int_t i{}; i<500; i++)
		for (Int_t j{}; j<wfD.maxWfLen; j++)
			hist2D[0][0]->SetBinContent(j+1, i+1, wfD.wfArr[i][j]);
	
	
	hist2D[0][0]->GetXaxis()->SetTitle("Timestamp (time bin #)");
	hist2D[0][0]->GetYaxis()->SetTitle( Form("Entries from Wf#%i",Int_t(wfD.wf_Num)) );
	
	//change to allTrig0 canvas, set y-axis to log-scale, create axis titles, and draw histogram
	gui->selCanvas(13, 0, 0);
	hist2D[0][0]->Draw("colz");
	
	// plotting the events individually
	gui->selCanvas(13,1,0);
	// Int_t start = 1;
	// hist2D[0][0]->ProjectionX("proj_x",start,start)->Draw("P");
	// for (Int_t eventval=2; eventval<500; eventval++){
		// hist2D[0][0]->ProjectionX("proj_x",eventval,eventval)->Draw("P,Same");
	// }
	UShort_t minval=0; minval--;
	UShort_t maxval = 0;
	for (Int_t i=0; i<500; i++){
		if(!wfD.hproj[i])
			wfD.hproj[i]= new TH1D( Form("hproj%i",i),"Superimposed Wfs",Int_t(wfD.maxWfLen),0,Double_t(wfD.maxWfLen) );
		for (Int_t j=0;j<wfD.maxWfLen;j++){
			wfD.hproj[i]->SetBinContent(j+1, wfD.wfArr[i][j]);
			if(wfD.wfArr[i][j]<minval) minval=wfD.wfArr[i][j];
			if(wfD.wfArr[i][j]>maxval) maxval=wfD.wfArr[i][j];
		}
		wfD.hproj[i]->Draw("P,Same");
	}
	//set the y-axis range using the max and min values found above
	UShort_t range = maxval-minval; 
	Double_t percent = 0.01;
	wfD.hproj[0]->GetYaxis()->SetRangeUser(minval-percent*range, maxval+percent*range);

	//updating the canvas
	gui->updateCanvas(13,0);
	gui->updateCanvas(13,1);

}






