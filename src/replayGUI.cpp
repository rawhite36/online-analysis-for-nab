//GUI


#include "../inc/replayGUI.hpp"
#include "../inc/replay.hpp"
#include <iostream>
#include <TApplication.h>
#include <TGMenu.h>
#include <TGTab.h>
#include <TGNumberEntry.h>
#include <TPaveLabel.h>
#include <TPolyLine.h>

ReplayGUI::ReplayGUI()
{
  //create main window and connect exit function to exit button
	mainWindow = {new TGMainFrame{gClient->GetRoot(), 1067, 600}};
	mainWindow->Connect("CloseWindow()","ReplayGUI",this,"exitProtocol()");

  //create drop down menu bar
  menuBar = {new TGMenuBar{mainWindow, 1067, 20, kHorizontalFrame}};
	mainWindow->AddFrame(menuBar);
  menuFile = {new TGPopupMenu{menuBar}};
	layout.push_back(new TGLayoutHints{kLHintsNormal, 0, 4, 0, 0});
  menuBar->AddPopup("&File", menuFile, layout.back());
  menuOpt = {new TGPopupMenu{menuBar}};
  menuBar->AddPopup("O&ptions", menuOpt, layout.back());
  menuHelp = {new TGPopupMenu{menuBar}};
	layout.push_back(new TGLayoutHints{});
  menuBar->AddPopup("&Help", menuHelp, layout.back());
}

//-------------------------------------------------------------------------------------------------

ReplayGUI::~ReplayGUI()
{
	for(Short_t i{0}; i < numBox.size(); ++i)
		for(Short_t j{0}; j < numBox[i].size(); ++j)
			numBox[i][j]->SetCleanup(kNoCleanup);
	for(Short_t i{0}; i < frame.size(); ++i)
		for(Short_t j{0}; j < frame[i].size(); ++j)
			frame[i][j]->SetCleanup(kNoCleanup);
	for(Short_t i{0}; i < frameAuto.size(); ++i)
		for(Short_t j{0}; j < frameAuto[i].size(); ++j)
			frameAuto[i][j]->SetCleanup(kNoCleanup);
	if(tabs)
		tabs->SetCleanup(kNoCleanup);
	menuBar->SetCleanup(kNoCleanup);
	mainWindow->SetCleanup(kNoCleanup);

	//cleanup
	for(Short_t i{0}; i < layout.size(); ++i)
		delete layout[i];
	if(hexShape)
		delete hexShape;
	for(Short_t i{0}; i < label.size(); ++i)
		for(Short_t j{0}; j < label[i].size(); ++j)
			delete label[i][j];
	for(Short_t i{0}; i < canvas.size(); ++i)
		for(Short_t j{0}; j < canvas[i].size(); ++j)
			delete canvas[i][j];
	for(Short_t i{0}; i < textButton.size(); ++i)
		for(Short_t j{0}; j < textButton[i].size(); ++j)
			delete textButton[i][j];
	for(Short_t i{0}; i < numBox.size(); ++i)
		for(Short_t j{0}; j < numBox[i].size(); ++j)
			delete numBox[i][j];
	for(Short_t i{0}; i < frame.size(); ++i)
		for(Short_t j{0}; j < frame[i].size(); ++j)
			delete frame[i][j];
	if(tabs)
		delete tabs;
  delete menuFile;
  delete menuOpt;
  delete menuHelp;
  delete menuBar;
  delete mainWindow;
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::exitProtocol()
{
	//terminate current instance of TApplication
	mainWindow->DontCallClose();
  gApplication->Terminate();
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::accessData(Replay* repTemp)
{
	//gain access to current instance of Replay
  rep = {repTemp};
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::createTabs()
{
  //create instance of TGTab
	tabs = {new TGTab{mainWindow, 1067, 600}};

  //create tabs
	createMain();
	createAllTrig0();
	createProton0();
	createProton1();
	createElectron0();
	createElectron1();
	createOther0();
	createOther1();
	createPair0();
	createPair1();
	createPair2();
	createPair3();
	createAExtraction();
	createWfs(); //LDM3

  //add tabs to parent and display main window and all of it's content
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsExpandY});
	mainWindow->AddFrame(tabs, layout.back());
	mainWindow->SetWindowName("Online Analysis");
	mainWindow->MapSubwindows();
	mainWindow->Resize(mainWindow->GetDefaultSize());
	mainWindow->MapWindow();
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::createMain()
{
	frameAuto.push_back(std::vector<TGCompositeFrame*>{});
	frame.push_back(std::vector<TGCompositeFrame*>{});
	canvas.push_back(std::vector<TRootEmbeddedCanvas*>{});
	numBox.push_back(std::vector<TGNumberEntry*>{});
	textButton.push_back(std::vector<TGTextButton*>{});
	label.push_back(std::vector<TPaveLabel*>{});

	//instantiate frames and objects that will be added to a frame, and add all frames and objects to
	//their parent
	frameAuto.back().push_back(tabs->AddTab("mainTab"));
  frame.back().push_back(new TGHorizontalFrame{frameAuto.back().back(), 1067, 400});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsExpandY|kLHintsTop});
  
  //add frame for "plotMain0" //LDM3
  frameAuto.back().back()->AddFrame(frame.back().back(), layout.back());
	canvas.back().push_back(new TRootEmbeddedCanvas{"plotMain0", frame.back().back(), 1057, 390});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsExpandY, 5, 5, 5, 5});
    //add correspnding canvas
    frame.back().back()->AddFrame(canvas.back().back(), layout.back());
	frame.back().push_back(new TGHorizontalFrame{frameAuto.back().back(), 1067, 200});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsBottom});
  
  //add frame for "statsMain" //LDM3
  frameAuto.back().back()->AddFrame(frame.back().back(), layout.back());
	canvas.back().push_back(new TRootEmbeddedCanvas{"statsMain", frame.back().back(), 857, 195});
	layout.push_back(new TGLayoutHints{kLHintsExpandX, 5, 5, 0, 5});
    //add corresponding canvas //LDM3
    frame.back().back()->AddFrame(canvas.back().back(), layout.back());
	frame.back().push_back(new TGVerticalFrame{frame.back().back(), 200, 195});
	layout.push_back(new TGLayoutHints(0));
  
  //not sure what this does... //LDM3
  (*(&frame.back().back() - 1))->AddFrame(frame.back().back(), layout.back());
  frame.back().push_back(new TGHorizontalFrame{frame.back().back(), 200, 60});
	layout.push_back(new TGLayoutHints{0, 0, 5, 5, 5});
	
  //add frame for the board label //LDM3
	(*(&frame.back().back() - 1))->AddFrame(frame.back().back(), layout.back());
	canvas.back().push_back(new TRootEmbeddedCanvas{"bdLabel", frame.back().back(), 55, 23});
	layout.push_back(new TGLayoutHints{kLHintsLeft|kLHintsCenterY, 0, 5});
	//add corresponding bdLabel canvas //LDM3
	frame.back().back()->AddFrame(canvas.back().back(), layout.back());
	//defining the number box for bd selection //LDM3
    numBox.back().push_back(new TGNumberEntry{frame.back().back(), 0, 9, -1, TGNumberFormat::kNESInteger,
	TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 5});
	layout.push_back(new TGLayoutHints{kLHintsRight|kLHintsCenterY});
	//adding this number box to the frame //LDM3
	frame.back().back()->AddFrame(numBox.back().back(), layout.back());
  frame.back().push_back(new TGHorizontalFrame{*(&frame.back().back() - 1), 200, 60});
	layout.push_back(new TGLayoutHints{0, 0, 5, 0, 5});
	
  //add frame for the channel label	 //LDM3
	(*(&frame.back().back() - 2))->AddFrame(frame.back().back(), layout.back());
	canvas.back().push_back(new TRootEmbeddedCanvas{"chLabel", frame.back().back(), 55, 23});
	//add the canvas for this //LDM3
	frame.back().back()->AddFrame(canvas.back().back(), *(&layout.back() - 2));
	//defining the number box for this  //LDM3
	numBox.back().push_back(new TGNumberEntry{frame.back().back(), 0, 9, -1, TGNumberFormat::kNESInteger,
	TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 7});
	//adding this number box to the frame //LDM3
	frame.back().back()->AddFrame(numBox.back().back(), *(&layout.back() - 1));
  frame.back().push_back(new TGHorizontalFrame{*(&frame.back().back() - 2), 200, 60});
	
  //adding in the text buttons //LDM3
	(*(&frame.back().back() - 3))->AddFrame(frame.back().back(), layout.back());
  textButton.back().push_back(new TGTextButton{frame.back().back(), "Select All Pix."});
	frame.back().back()->AddFrame(textButton.back().back(), *(&layout.back() - 2));
  textButton.back().push_back(new TGTextButton{frame.back().back(), "Select Pix."});
	frame.back().back()->AddFrame(textButton.back().back(), *(&layout.back() - 1));
  frame.back().push_back(new TGHorizontalFrame{*(&frame.back().back() - 3), 200, 60});
	
  //adding frame for the runnum //LDM3
	(*(&frame.back().back() - 4))->AddFrame(frame.back().back(), layout.back());
	canvas.back().push_back(new TRootEmbeddedCanvas{"runLabel", frame.back().back(), 55, 23});
	//add the corresponding canvas //LDM3
	frame.back().back()->AddFrame(canvas.back().back(), *(&layout.back() - 2));
	//defining the num box //LDM3
    numBox.back().push_back(new TGNumberEntry{frame.back().back(), static_cast<Double_t>(rep->runNumLast), 9, -1, TGNumberFormat::kNESInteger,
	TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0., 1e7});
	//add num box to the frame //LDM3
	frame.back().back()->AddFrame(numBox.back().back(), *(&layout.back() - 1));
  frame.back().push_back(new TGHorizontalFrame{*(&frame.back().back() - 4), 200, 60});
	
  //adding the "Select Latest Run" text button //LDM3
	(*(&frame.back().back() - 5))->AddFrame(frame.back().back(), layout.back());
  textButton.back().push_back(new TGTextButton{frame.back().back(), "Select Latest Run"});
	frame.back().back()->AddFrame(textButton.back().back(), *(&layout.back() - 2));

	//connect user-input to member functions (i.e signals to slots)
  (*(&numBox.back().back() - 2))->Connect("ValueSet(Long_t)", "ReplayGUI", this, "selPixel()");
  (*(&numBox.back().back() - 2))->GetNumberEntry()->Connect("ReturnPressed()", "ReplayGUI", this, "selPixel()");
  
  (*(&numBox.back().back() - 1))->Connect("ValueSet(Long_t)", "ReplayGUI", this, "selPixel()");
  (*(&numBox.back().back() - 1))->GetNumberEntry()->Connect("ReturnPressed()", "ReplayGUI", this, "selPixel()");
  
  (*(&textButton.back().back() - 2))->Connect("Clicked()", "ReplayGUI", this, "selAllPixels()");
  (*(&textButton.back().back() - 1))->Connect("Clicked()", "ReplayGUI", this, "selPixel()");
  
  textButton.back().back()->Connect("Clicked()", "ReplayGUI", this, "selLatestRun()");
  
  //LDM3: this one allows the program to automatically re-load when the directional up and down buttons are pressed
  //numBox.back().back()->Connect("ValueSet(Long_t)", "ReplayGUI", this, "selRun()");
  //LDM3: run only the function below if you don't want the function to reload only when you press enter
  numBox.back().back()->GetNumberEntry()->Connect("ReturnPressed()", "ReplayGUI", this, "selRun()");

	//instantiate objects that will be drawn to a canvas
	label.back().push_back(new TPaveLabel{0., 0., 1., 1., "Board", "tl"});
	label.back().push_back(new TPaveLabel{0., 0., 1., 1., "Channel", "tl"});
	label.back().push_back(new TPaveLabel{0., 0., 1., 1., "Run", "tl"});

	//alter canvases
	(*(&canvas.back().back() - 4))->GetCanvas()->Divide(2, 1);
	(*(&label.back().back() - 2))->SetTextSize(0.63);
	(*(&label.back().back() - 2))->SetTextFont(42);
	(*(&label.back().back() - 2))->SetTextAlign(22);
	(*(&label.back().back() - 1))->SetTextSize(0.63);
	(*(&label.back().back() - 1))->SetTextFont(42);
	(*(&label.back().back() - 1))->SetTextAlign(22);
	label.back().back()->SetTextSize(0.63);
	label.back().back()->SetTextFont(42);
	label.back().back()->SetTextAlign(22);
	(*(&canvas.back().back() - 2))->GetCanvas()->cd();
  (*(&label.back().back() - 2))->Draw();
	(*(&canvas.back().back() - 1))->GetCanvas()->cd();
  (*(&label.back().back() - 1))->Draw();
	canvas.back().back()->GetCanvas()->cd();
  label.back().back()->Draw();
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::createAllTrig0()
{
	frameAuto.push_back(std::vector<TGCompositeFrame*>{});
	canvas.push_back(std::vector<TRootEmbeddedCanvas*>{});

	//instantiate frames and objects that will be added to a frame, and add all frames and objects to
	//their parent
	frameAuto.back().push_back(tabs->AddTab("allTrig0Tab"));
	canvas.back().push_back(new TRootEmbeddedCanvas{"allTrig0Can0", frameAuto.back().back(), 1067, 300});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsExpandY|kLHintsTop});
	frameAuto.back().back()->AddFrame(canvas.back().back(), layout.back());
	canvas.back().push_back(new TRootEmbeddedCanvas{"allTrig0Can1", frameAuto.back().back(), 1067, 300});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsExpandY|kLHintsBottom});
	frameAuto.back().back()->AddFrame(canvas.back().back(), layout.back());

	//alter canvases
	(*(&canvas.back().back() - 1))->GetCanvas()->Divide(2, 1, .01, .01);
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::createProton0()
{
	frameAuto.push_back(std::vector<TGCompositeFrame*>{});
	canvas.push_back(std::vector<TRootEmbeddedCanvas*>{});

	//instantiate frames and objects that will be added to a frame, and add all frames and objects to
	//their parent
	frameAuto.back().push_back(tabs->AddTab("proton0Tab"));
	canvas.back().push_back(new TRootEmbeddedCanvas{"proton0Can0", frameAuto.back().back(), 1067, 600});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsExpandY});
	frameAuto.back().back()->AddFrame(canvas.back().back(), layout.back());

	//alter canvases
	canvas.back().back()->GetCanvas()->Divide(2, 2, .01, .01);
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::createProton1()
{
	frameAuto.push_back(std::vector<TGCompositeFrame*>{});
	canvas.push_back(std::vector<TRootEmbeddedCanvas*>{});

	//instantiate frames and objects that will be added to a frame, and add all frames and objects to
	//their parent
	frameAuto.back().push_back(tabs->AddTab("proton1Tab"));
	canvas.back().push_back(new TRootEmbeddedCanvas{"proton1Can0", frameAuto.back().back(), 1067, 600});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsExpandY});
	frameAuto.back().back()->AddFrame(canvas.back().back(), layout.back());

	//alter canvases
	canvas.back().back()->GetCanvas()->Divide(2, 2, .01, .01);
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::createElectron0()
{
	frameAuto.push_back(std::vector<TGCompositeFrame*>{});
	canvas.push_back(std::vector<TRootEmbeddedCanvas*>{});

	//instantiate frames and objects that will be added to a frame, and add all frames and objects to
	//their parent
	frameAuto.back().push_back(tabs->AddTab("electron0Tab"));
	canvas.back().push_back(new TRootEmbeddedCanvas{"electron0Can0", frameAuto.back().back(), 1067, 600});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsExpandY});
	frameAuto.back().back()->AddFrame(canvas.back().back(), layout.back());

	//alter canvases
	canvas.back().back()->GetCanvas()->Divide(2, 2, .01, .01);
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::createElectron1()
{
	frameAuto.push_back(std::vector<TGCompositeFrame*>{});
	canvas.push_back(std::vector<TRootEmbeddedCanvas*>{});

	//instantiate frames and objects that will be added to a frame, and add all frames and objects to
	//their parent
	frameAuto.back().push_back(tabs->AddTab("electron1Tab"));
	canvas.back().push_back(new TRootEmbeddedCanvas{"electron1Can0", frameAuto.back().back(), 1067, 600});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsExpandY});
	frameAuto.back().back()->AddFrame(canvas.back().back(), layout.back());

	//alter canvases
	canvas.back().back()->GetCanvas()->Divide(1, 2, .01, .01);
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::createOther0()
{
	frameAuto.push_back(std::vector<TGCompositeFrame*>{});
	canvas.push_back(std::vector<TRootEmbeddedCanvas*>{});

	//instantiate frames and objects that will be added to a frame, and add all frames and objects to
	//their parent
	frameAuto.back().push_back(tabs->AddTab("other0Tab"));
	canvas.back().push_back(new TRootEmbeddedCanvas{"other0Can0", frameAuto.back().back(), 1067, 600});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsExpandY});
	frameAuto.back().back()->AddFrame(canvas.back().back(), layout.back());

	//alter canvases
	canvas.back().back()->GetCanvas()->Divide(2, 2, .01, .01);
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::createOther1()
{
	frameAuto.push_back(std::vector<TGCompositeFrame*>{});
	canvas.push_back(std::vector<TRootEmbeddedCanvas*>{});

	//instantiate frames and objects that will be added to a frame, and add all frames and objects to
	//their parent
	frameAuto.back().push_back(tabs->AddTab("other1Tab"));
	canvas.back().push_back(new TRootEmbeddedCanvas{"other1Can0", frameAuto.back().back(), 1067, 600});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsExpandY});
	frameAuto.back().back()->AddFrame(canvas.back().back(), layout.back());
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::createPair0()
{
	frameAuto.push_back(std::vector<TGCompositeFrame*>{});
	canvas.push_back(std::vector<TRootEmbeddedCanvas*>{});

	//instantiate frames and objects that will be added to a frame, and add all frames and objects to
	//their parent
	frameAuto.back().push_back(tabs->AddTab("pair0Tab"));
	canvas.back().push_back(new TRootEmbeddedCanvas{"pair0Can0", frameAuto.back().back(), 1067, 600});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsExpandY});
	frameAuto.back().back()->AddFrame(canvas.back().back(), layout.back());

	//alter canvases
	canvas.back().back()->GetCanvas()->Divide(1, 2, .01, .01);
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::createPair1()
{
	frameAuto.push_back(std::vector<TGCompositeFrame*>{});
	canvas.push_back(std::vector<TRootEmbeddedCanvas*>{});

	//instantiate frames and objects that will be added to a frame, and add all frames and objects to
	//their parent
	frameAuto.back().push_back(tabs->AddTab("pair1Tab"));
	canvas.back().push_back(new TRootEmbeddedCanvas{"pair1Can0", frameAuto.back().back(), 1067, 600});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsExpandY});
	frameAuto.back().back()->AddFrame(canvas.back().back(), layout.back());
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::createPair2()
{
	frameAuto.push_back(std::vector<TGCompositeFrame*>{});
	canvas.push_back(std::vector<TRootEmbeddedCanvas*>{});

	//instantiate frames and objects that will be added to a frame, and add all frames and objects to
	//their parent
	frameAuto.back().push_back(tabs->AddTab("pair2Tab"));
	canvas.back().push_back(new TRootEmbeddedCanvas{"pair2Can0", frameAuto.back().back(), 1067, 600});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsExpandY});
	frameAuto.back().back()->AddFrame(canvas.back().back(), layout.back());

	//alter canvases
	canvas.back().back()->GetCanvas()->Divide(3, 2, .01, .01);
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::createPair3()
{
	frameAuto.push_back(std::vector<TGCompositeFrame*>{});
	canvas.push_back(std::vector<TRootEmbeddedCanvas*>{});

	//instantiate frames and objects that will be added to a frame, and add all frames and objects to
	//their parent
	frameAuto.back().push_back(tabs->AddTab("pair3Tab"));
	canvas.back().push_back(new TRootEmbeddedCanvas{"pair3Can0", frameAuto.back().back(), 1067, 600});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsExpandY});
	frameAuto.back().back()->AddFrame(canvas.back().back(), layout.back());

	//alter canvases
	canvas.back().back()->GetCanvas()->Divide(1, 2, .01, .01);
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::createAExtraction()
{
	frameAuto.push_back(std::vector<TGCompositeFrame*>{});
	canvas.push_back(std::vector<TRootEmbeddedCanvas*>{});

	//instantiate frames and objects that will be added to a frame, and add all frames and objects to
	//their parent
	frameAuto.back().push_back(tabs->AddTab("aExtractionTab"));
	canvas.back().push_back(new TRootEmbeddedCanvas{"aExtractionCan0", frameAuto.back().back(), 1067, 600});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsExpandY});
	frameAuto.back().back()->AddFrame(canvas.back().back(), layout.back());

	//alter canvases
	canvas.back().back()->GetCanvas()->Divide(1, 2, .01, .01);
}
//-------------------------------------------------------------------------------------------------

//LDM3 //originally copied directly from ReplayGUI::createAExtraction
void ReplayGUI::createWfs()
{
	frameAuto.push_back(std::vector<TGCompositeFrame*>{});
	frame.push_back(std::vector<TGCompositeFrame*>{});
	canvas.push_back(std::vector<TRootEmbeddedCanvas*>{});
	numBox.push_back(std::vector<TGNumberEntry*>{});
	textButton.push_back(std::vector<TGTextButton*>{});
	label.push_back(std::vector<TPaveLabel*>{});

	//instantiate frames and objects that will be added to a frame, and add all frames and objects to
	//their parent
	frameAuto.back().push_back(tabs->AddTab("WaveformsTab"));
	frame.back().push_back(new TGHorizontalFrame{frameAuto.back().back(), 1067, 400});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsExpandY|kLHintsTop});
  
	//add frame for "2Dwfs"
	frameAuto.back().back()->AddFrame(frame.back().back(), layout.back());
	//add slots for new canvas and layout
	canvas.back().push_back(new TRootEmbeddedCanvas{"2Dwfs", frame.back().back(), 1057, 390});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsExpandY, 5, 5, 5, 5});
    //add this new canvas to the latest frame
    frame.back().back()->AddFrame(canvas.back().back(), layout.back());
	//add new slots for another frame and layout
	frame.back().push_back(new TGHorizontalFrame{frameAuto.back().back(), 1067, 200});
	layout.push_back(new TGLayoutHints{kLHintsExpandX|kLHintsBottom});
  
  //add frame for "wf projection viewer"
  frameAuto.back().back()->AddFrame(frame.back().back(), layout.back());
	canvas.back().push_back(new TRootEmbeddedCanvas{"wfs projection", frame.back().back(), 857, 195});
	layout.push_back(new TGLayoutHints{kLHintsExpandX, 5, 5, 0, 5});
    //add corresponding canvas
    frame.back().back()->AddFrame(canvas.back().back(), layout.back());
	frame.back().push_back(new TGVerticalFrame{frame.back().back(), 200, 195});
	layout.push_back(new TGLayoutHints(0));

  //add frame for the starting wf num
 	(*(&frame.back().back() - 1))->AddFrame(frame.back().back(), layout.back());
	canvas.back().push_back(new TRootEmbeddedCanvas{"wflabel", frame.back().back(), 55, 23});
	layout.push_back(new TGLayoutHints{kLHintsLeft|kLHintsCenterY, 0, 5});
	//add corresponding bdLabel canvas
	frame.back().back()->AddFrame(canvas.back().back(), layout.back());
	//defining the number box for bd selection
    numBox.back().push_back(new TGNumberEntry{frame.back().back(), 0, 9, -1, TGNumberFormat::kNESInteger,
		TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 1e7});
	layout.push_back(new TGLayoutHints{kLHintsRight|kLHintsCenterY});
	//adding this number box to the frame
	frame.back().back()->AddFrame(numBox.back().back(), layout.back());
  
	//connect user-input to member functions (i.e signals to slot
	//numBox.back().back()->Connect("ValueSet(Long_t)", "ReplayGUI", this, "changeWfNum()");
	numBox.back().back()->GetNumberEntry()->Connect("ReturnPressed()", "ReplayGUI", this, "changeWfNum()");
 
	//instantiate objects that will be drawn to a canvas
	label.back().push_back(new TPaveLabel{0., 0., 1., 1., "Starting Wf#", "tl"});

	canvas.back().back()->GetCanvas()->cd();
		label.back().back()->Draw();

	
}
//-------------------------------------------------------------------------------------------------

void ReplayGUI::changeWfNum(){
	rep->wfD.readData(rep->tD.numEnt, numBox[1][0]->GetNumberEntry()->GetIntNumber() ); 
	rep->plotWaveforms();
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::selPixel()
{
	//instantiate and initialize variables
	const Double_t hexFact{sqrt(3.)/2.};
	const Double_t hexSide{.042};
	Char_t bdTemp{static_cast<Char_t>(numBox[0][0]->GetNumberEntry()->GetIntNumber())};
	Char_t chTemp{static_cast<Char_t>(numBox[0][1]->GetNumberEntry()->GetIntNumber())};
	Double_t binCoord[2][7];
	if(rep->bdChPixel[8*bdTemp + chTemp] != 0)
	{
		if(rep->bdChPixel[8*bdTemp + chTemp] < 128)
		{
			binCoord[0][0] = {rep->binCenter[0][rep->bdChPixel[8*bdTemp + chTemp] - 1] - hexSide/2.};
			binCoord[1][0] = {rep->binCenter[1][rep->bdChPixel[8*bdTemp + chTemp] - 1] - hexSide*hexFact};
		}
		else
		{
			binCoord[0][0] = {rep->binCenter[0][rep->bdChPixel[8*bdTemp + chTemp] - 129] - hexSide/2.};
			binCoord[1][0] = {rep->binCenter[1][rep->bdChPixel[8*bdTemp + chTemp] - 129] - hexSide*hexFact};
		}
		binCoord[0][1] = {binCoord[0][0] + hexSide};
		binCoord[1][1] = {binCoord[1][0]};
		binCoord[0][2] = {binCoord[0][1] + hexSide/2.};
		binCoord[1][2] = {binCoord[1][1] + hexSide*hexFact};
		binCoord[0][3] = {binCoord[0][1]};
		binCoord[1][3] = {binCoord[1][2] + hexSide*hexFact};
		binCoord[0][4] = {binCoord[0][0]};
		binCoord[1][4] = {binCoord[1][3]};
		binCoord[0][5] = {binCoord[0][4] - hexSide/2.};
		binCoord[1][5] = {binCoord[1][2]};
		binCoord[0][6] = {binCoord[0][0]};
		binCoord[1][6] = {binCoord[1][0]};
	}

	//select pixel
	if(hexShape)
		delete hexShape;
	if(rep->bdChPixel[8*bdTemp + chTemp])
	{
		hexShape = {new TPolyLine{7, binCoord[0], binCoord[1]}};
		hexShape->SetLineColor(2);
		hexShape->SetLineWidth(4);
		if(rep->bdChPixel[8*bdTemp + chTemp] < 128)
			canvas[0][0]->GetCanvas()->cd(1);
		else
			canvas[0][0]->GetCanvas()->cd(2);
		hexShape->Draw("same");
	}
	else
		hexShape = {};
  canvas[0][0]->GetCanvas()->Update();
	rep->fillStats(bdTemp, chTemp);
	canvas[0][1]->GetCanvas()->Update();
  rep->plotAllTrig0(bdTemp, chTemp);
  canvas[1][0]->GetCanvas()->Update();
  canvas[1][1]->GetCanvas()->Update();
	rep->plotProton0(bdTemp, chTemp);
  canvas[2][0]->GetCanvas()->Update();
	rep->plotProton1(bdTemp, chTemp);
  canvas[3][0]->GetCanvas()->Update();
	rep->plotElectron0(bdTemp, chTemp);
  canvas[4][0]->GetCanvas()->Update();
	rep->plotElectron1(bdTemp, chTemp);
  canvas[5][0]->GetCanvas()->Update();
	rep->plotOther0(bdTemp, chTemp);
  canvas[6][0]->GetCanvas()->Update();
	rep->plotOther1(bdTemp, chTemp);
  canvas[7][0]->GetCanvas()->Update();
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::selAllPixels()
{
	//select all pixels
	if(hexShape)
	{
		delete hexShape;
		hexShape = {};
		canvas[0][0]->GetCanvas()->Update();
	}
	rep->fillStats(6, 0);
	canvas[0][1]->GetCanvas()->Update();
  rep->plotAllTrig0(6, 0);
  canvas[1][0]->GetCanvas()->Update();
  canvas[1][1]->GetCanvas()->Update();
	rep->plotProton0(6, 0);
  canvas[2][0]->GetCanvas()->Update();
	rep->plotProton1(6, 0);
  canvas[3][0]->GetCanvas()->Update();
	rep->plotElectron0(6, 0);
  canvas[4][0]->GetCanvas()->Update();
	rep->plotElectron1(6, 0);
  canvas[5][0]->GetCanvas()->Update();
	rep->plotOther0(6, 0);
  canvas[6][0]->GetCanvas()->Update();
	rep->plotOther1(6, 0);
  canvas[7][0]->GetCanvas()->Update();
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::selRun()
{
	//extract data from data files
  	rep->readTrigFile(numBox[0][2]->GetNumberEntry()->GetIntNumber());
  	rep->readWfFile(numBox[0][2]->GetNumberEntry()->GetIntNumber()); //LDM3
  	rep->readParamsFile(numBox[0][2]->GetNumberEntry()->GetIntNumber());//LDM3

 	//assure that no errors have occured before proceeding
	if(!rep->reportError())
	{
		//process data
		rep->partIdent();
		rep->computeProtData();
		rep->extractCorPar();

		//fill GUI
		rep->fillGUI();
	}
  else
  {
		//send error message to stdout
		if(rep->reportError() & 1)
			std::cout << "Error: Cannot Open Trigger Data File\nProceeding with "
			"Previous File: " << rep->tD.fileName << '\n';

		rep->errorFlag = {};
  }
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::selLatestRun()
{
	rep->updateFileInfo();

	//extract data from trigger file
  rep->readTrigFile(rep->runNumLast);

	//assure that no errors have occured before proceeding
	if(!rep->reportError())
	{
	  numBox.back().back()->SetIntNumber(rep->runNumLast);

		//process data
		rep->partIdent();
		rep->computeProtData();
		rep->extractCorPar();

		//fill GUI
		rep->fillGUI();
	}
  else
  {
		//send error message to stdout
		if(rep->reportError() & 1)
			std::cout << "Error: Cannot Open Trigger Data File\nProceeding with "
			"Previous File: " << rep->tD.fileName << '\n';

		rep->errorFlag = {};
  }
}
