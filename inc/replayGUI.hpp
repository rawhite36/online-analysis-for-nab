//class header


#ifndef REPLAY_GUI_HPP
#define REPLAY_GUI_HPP

#include <vector>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TQObject.h>
#include <RQ_OBJECT.h>

class Replay;
class TGMainFrame;
class TGMenuBar;
class TGPopupMenu;
class TGTab;
class TGHorizontalFrame;
class TGVerticalFrame;
class TGNumberEntry;
class TGTextButton;
class TPaveLabel;
class TPolyLine;

class ReplayGUI
{
	//execute RQ_OBJECT macro
	RQ_OBJECT("ReplayGUI");

	//create member variables
	std::vector<TGLayoutHints*> layout{};
	TGMainFrame* mainWindow;
  TGMenuBar* menuBar;
  TGPopupMenu* menuFile;
  TGPopupMenu* menuOpt;
  TGPopupMenu* menuHelp;
	TGTab* tabs{};
  std::vector<std::vector<TGCompositeFrame*>> frame{};
	std::vector<std::vector<TGCompositeFrame*>> frameAuto{};
	std::vector<std::vector<TRootEmbeddedCanvas*>> canvas{};
  std::vector<std::vector<TGNumberEntry*>> numBox{};
  std::vector<std::vector<TGTextButton*>> textButton{};
	std::vector<std::vector<TPaveLabel*>> label{};
	TPolyLine *hexShape{};
  Replay* rep{};

	//declare member functions
	void createMain();
	void createAllTrig0();
	void createProton0();
	void createProton1();
	void createElectron0();
	void createElectron1();
	void createOther0();
	void createOther1();
	void createPair0();
	void createPair1();
	void createPair2();
	void createPair3();
	void createAExtraction();
	void createWfs(); //LDM3

	public:
		//declare constructor and destructor
		ReplayGUI();
		~ReplayGUI();

		//declare member functions
		void exitProtocol();
		void accessData(Replay*);
		void createTabs();
		void selPixel();
		void selAllPixels();
		void selRun();
		void selLatestRun();
		void changeWfNum();
		inline void selCanvas(Short_t, Short_t, Short_t);
		inline void updateCanvas(Short_t, Short_t);
};

//-------------------------------------------------------------------------------------------------

void ReplayGUI::selCanvas(Short_t x, Short_t y, Short_t z)
{
	//select canvas and subpad
	canvas[x][y]->GetCanvas()->cd(z);
}

//-------------------------------------------------------------------------------------------------

void ReplayGUI::updateCanvas(Short_t x, Short_t y)
{
	//update canvas
	canvas[x][y]->GetCanvas()->Update();
}

#endif
