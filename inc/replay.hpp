//Replay class header


#ifndef REPLAY_HPP
#define REPLAY_HPP

#include <string>
#include <vector>
#include <RtypesCore.h>

class ReplayGUI;
class TH1;
class TH1D;
class TH2D; //LDM3 for the 2D plots
class TF1;
class TStyle;
class TPaveText;

class Replay
{
	//declare friends
	friend class ReplayGUI;

	//declare member structures
	struct trigData
	{
		//instantiate member variables
		std::string dirName{};
		std::string fileName{};
		Long64_t numEnt{};
		Int_t* bdNum{};
		UChar_t* chNum{};
		ULong64_t* timeReq{};
		UShort_t* adcVal{};

		//declare destructor
		~trigData();

		//declare member functions
		void readData();
	} tD{};
	
	struct waveformData{ //LDM3
		std::string dirName{};
		std::string fileName{};
		
		Long64_t wf_Num{};
		
		Bool_t result{};
		UInt_t eventId{};
		UChar_t bdCh{};
		ULong_t timestamp{};
		ULong_t timeReq{};
		UShort_t waveSource{};
		
		UInt_t * wfLenArr{}; //500 length array
		UInt_t wfLen{}; //just a single variable, not the wfLenArr
		UInt_t maxWfLen{};
		UShort_t* wfArr[500]{}; //500 = number of events to be displaying

		TH1D *hproj[500]{};
		
		//declare desctructor
		~waveformData();
		
		//declare member functions
		void readData(Long64_t numEnt, Long64_t wfNum={-1});		
		
	} wfD{};
	
	struct paramsData{ //LDM3
		std::string dirName{};
		std::string fileName{};
		
		UChar_t bcPixArr[256]{};
		
		//declar member functions
		void readData();
	} paramsD{};

	struct partData
	{
		//instantiate member variables
		Long64_t numEnt{};
		Int_t* bdNum[2]{};
		UChar_t* chNum[2]{};
		ULong64_t* timeReq[2]{};
		UShort_t* adcVal[2]{};
		Short_t* partNum[2]{};
		Double_t* protMom{};
		Double_t* cosTheta{};
		
		Bool_t flag = false; //LDM3 used to prevent crashing from the manitoba test data

		//declare destructor
		~partData();

		//declare member functions
		void setData(Long64_t);
	} pD{};

	//instantiate member variables
	static UChar_t errorFlag;
	const Double_t massProt{938272.};
	const Double_t massElec{510.999};
	const Double_t qValue{782.3};
	const Double_t spdLight{2.99792e8};
	const Double_t geantSimA{-.103};
	const Double_t calib{1.};
	ULong64_t* partIndex[2]{};
	Double_t binCenter[2][127];
	UChar_t bdChPixel[256]{};
  Double_t*** spectMap[3]{};
  Double_t** spectMapProj[3]{};
	TF1* linFitCorPar[39]{};
	Double_t valueCorPar[39][2];
	std::vector<Double_t> corParTot[39][2];
	std::vector<Long64_t> vCPTFlag;
	Double_t valCorParTot[39][2];
	std::vector<std::vector<TH1*>> hist{};

	//LDM3 for the waveform viewer... vector in case we want to add more 2D plots
	std::vector<std::vector<TH2D*>> hist2D{};
	
	std::vector<std::vector<TF1*>> funct{};
	std::vector<TStyle*> style{};
	std::vector<TPaveText*> text{};
	ReplayGUI* gui{};
	Char_t runFlag{};
	Long64_t runNum;
	Long64_t runNumLast;
	
	//declare analysis member functions
	Char_t pixRingSelect(Long64_t);
	void sigCompile();
	UChar_t findCosThTop(TH1D*, Double_t[2]);
	void compTotCorPar();
	void fillStats(Char_t, Char_t);

	//declare GUI-related member functions
	void plotMain();
	void plotAllTrig0(Char_t, Char_t);
	void plotProton0(Char_t, Char_t);
	void plotProton1(Char_t, Char_t);
	void plotElectron0(Char_t, Char_t);
	void plotElectron1(Char_t, Char_t);
	void plotOther0(Char_t, Char_t);
	void plotOther1(Char_t, Char_t);
	void plotPair0();
	void plotPair1();
	void plotPair2();
	void plotPair3();
	void plotAExtraction();
	void plotWaveforms(); //LDM3

  public:
		//declare constructor and destructor
    	Replay();
    	~Replay();

		//declare file-I/O member functions
		void setTrigDir(std::string dirNameTemp){
			tD.dirName = dirNameTemp + (dirNameTemp[dirNameTemp.size()-1]!='/'?"/":"");
			}
		void setWfDir(std::string dirNameTemp){ //LDM3
			wfD.dirName = dirNameTemp + (dirNameTemp[dirNameTemp.size()-1]!='/'?"/":"");
			}
		void setParamsDir(std::string dirNameTemp){ //LDM3
			paramsD.dirName = dirNameTemp + (dirNameTemp[dirNameTemp.size()-1]!='/'?"/":"");
		}
		void setPixelVal(Int_t index, Int_t pixel){bdChPixel[index] = pixel;} //LDM3
		Int_t getPixelVal(Int_t index){return bdChPixel[index];}
		std::string getTrigFile() {return tD.fileName;} 
		std::string getWaveformFile() {return wfD.fileName;} //LDM3
		Bool_t getpDflag() {return pD.flag;} //LDM3
		void updateFileInfo();
    	UChar_t readTrigFile(Long64_t = {-1});
    	UChar_t readWfFile(Long64_t = {-1}); //LDM3
    	UChar_t readParamsFile(Long64_t={-1}); //LDM3
		UChar_t readSpectMap(std::string);
    //UChar_t calibrate();  **  calibration member function is disabled

		//declare analysis member functions
		void partIdent();
		void computeProtData();
		void extractCorPar();

		//declare GUI-related member functions
		void createGUI();
		void fillGUI();
		
		//declare other member functions
		inline UChar_t reportError();
};

//-------------------------------------------------------------------------------------------------

UChar_t Replay::reportError()
{
	return errorFlag;
}

#endif
