

bin/nabOnline: build/main.o build/replay.o build/replayGUI.o build/replayGUI_Dict.o
	g++ -std=c++11 -o bin/nabOnline build/*.o `root-config --glibs`

build/main.o: src/main.cpp inc/replay.hpp inc/replayGUI.hpp
	g++ -c -std=c++11 -o build/main.o src/main.cpp `root-config --cflags`

build/replay.o: src/replay.cpp inc/replay.hpp inc/replayGUI.hpp
	g++ -c -std=c++11 -o build/replay.o src/replay.cpp `root-config --cflags`

build/replayGUI.o: src/replayGUI.cpp inc/replayGUI.hpp inc/replay.hpp
	g++ -c -std=c++11 -o build/replayGUI.o src/replayGUI.cpp `root-config --cflags`

build/replayGUI_Dict.o: inc/replayGUI.hpp inc/replayGUI_LinkDef.hpp
	cd src && \
	rootcling -f replayGUI_Dict.cpp -c ../inc/replayGUI.hpp ../inc/replayGUI_LinkDef.hpp && \
	mv replayGUI_Dict_rdict.pcm ../bin && cd .. && \
	g++ -c -std=c++11 -o build/replayGUI_Dict.o src/replayGUI_Dict.cpp `root-config --cflags`

clean:
	rm src/replayGUI_Dict.cpp build/* bin/*
